/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb_be_mngr.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipm_uci_commit.h"

#include "mod_ipm_uci_utils.h"

#define ME "ip-mod"

/**
 * @brief Replaces or adds the new IP address to the current list
 * @param section_name Name of the uci section on which the action should be performed
 * @param new_addr The new IP address that should be set
 * @param old_addr The address that should be replaced, when NULL the new_addr will be added to the list
 * @return amxc variant pointer to an amxc list containing the updated list of IP addresses
 */
static amxc_var_t* list_replace_ipaddrs(const char* section_name, const char* new_addr,
                                        const char* old_addr, const char* option) {
    amxc_var_t* uci_ret = uci_get_network_section(section_name, option);
    amxc_var_t* ip_list = GETP_ARG(uci_ret, "0.0");
    bool replace_action = true;

    if(ip_list == NULL) {
        amxc_var_t* tmp = amxc_var_add(amxc_llist_t, uci_ret, NULL);
        ip_list = amxc_var_add(amxc_llist_t, tmp, NULL);
    } else {
        amxc_var_cast(ip_list, AMXC_VAR_ID_LIST);
    }

    if((old_addr == NULL) || (*old_addr == '\0')) {
        replace_action = false;
    }

    amxc_var_for_each(addr, ip_list) {
        const char* addr_str = GET_CHAR(addr, NULL);
        if((addr_str == NULL) || (*addr_str == '\0')) {
            continue;
        }
        if(replace_action && (strncmp(addr_str, old_addr, INET6_ADDRSTRLEN) == 0)) {
            amxc_var_set_cstring_t(addr, new_addr);
            goto exit;
        }
        if(strncmp(addr_str, new_addr, INET6_ADDRSTRLEN) == 0) {
            SAH_TRACEZ_INFO(ME, "'%s' already set in '%s'", new_addr, section_name);
            amxc_var_delete(&uci_ret);
            goto exit;
        }
    }

    SAH_TRACEZ_INFO(ME, "Adding '%s' as a new address to '%s'", new_addr, section_name);
    amxc_var_add(cstring_t, ip_list, new_addr);

exit:
    return uci_ret;
}

/**
 * @brief Removes an IPv4/6 address from the current list
 * @param section_name Name of the uci section off which the address should be removed
 * @param rm_addr The IP address that should be removed
 * @return amxc variant pointer to an amxc list containing the updated list of IP addresses, NULL if list did not change
 */
static amxc_var_t* list_delete_ipaddr(const char* section_name,
                                      const char* rm_addr, const char* option) {
    amxc_var_t* uci_ret = uci_get_network_section(section_name, option);
    amxc_var_t* ip_list = GETP_ARG(uci_ret, "0.0");

    if(ip_list == NULL) {
        amxc_var_t* tmp = amxc_var_add(amxc_llist_t, uci_ret, NULL);
        ip_list = amxc_var_add(amxc_llist_t, tmp, NULL);
    } else {
        amxc_var_cast(ip_list, AMXC_VAR_ID_LIST);
    }

    amxc_var_for_each(addr, ip_list) {
        const char* addr_str = GET_CHAR(addr, NULL);
        if((addr_str == NULL) || (*addr_str == '\0')) {
            continue;
        }
        if(strncmp(addr_str, rm_addr, INET6_ADDRSTRLEN) == 0) {
            //remove the current address from the list in uci_ret
            amxc_var_delete(&addr);
            goto exit;
        }
    }

    // no match was found, clearing full list to indicate list did not change
    amxc_var_delete(&uci_ret);

exit:
    return uci_ret;
}

/**
 * @brief performs a ubus call uci get '{"config":"network","type":"interface","section":section}'
 * @param section interface name (lan, guest, ...)
 * @param option optional, when all options are wanted this should be set to NULL
 * @return amxc variant pointer to an amxc list containing a single element with a htable of the returned option(s)
 */
amxc_var_t* uci_get_network_section(const char* section_name, const char* option) {

    amxc_var_t uci_args;
    amxc_var_t* uci_ret;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&uci_ret);

    amxc_var_add_key(cstring_t, &uci_args, "config", "network");
    amxc_var_add_key(cstring_t, &uci_args, "type", "interface");
    amxc_var_add_key(cstring_t, &uci_args, "section", section_name);
    amxc_var_add_key(cstring_t, &uci_args, "option", option);

    uci_call("get", &uci_args, uci_ret, false);

    amxc_var_clean(&uci_args);
    return uci_ret;
}

int init_uci_args(const char* section_name, amxc_var_t* uci_args) {
    int rv = -1;
    amxc_var_t* uci_current = NULL;

    amxc_var_init(uci_args);

    when_null_trace(section_name, exit, ERROR, "can NOT extract sectionName from parameters");

    // Get current uci values of the network interface
    uci_current = uci_get_network_section(section_name, NULL);
    if(amxc_var_is_null(GETI_ARG(uci_current, 0))) {
        SAH_TRACEZ_WARNING(ME, "Could not find network section '%s' in uci!", section_name);
        goto exit;
    }

    amxc_var_set_type(uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, uci_args, "config", "network");
    amxc_var_add_key(cstring_t, uci_args, "type", "interface");
    amxc_var_add_key(cstring_t, uci_args, "section", section_name);

    rv = 0;
exit:
    amxc_var_delete(&uci_current);
    return rv;
}

/**
 * @brief Perform uci call specified by the method and follow it up with a 'delayed' commit.
 * @param method uci method; get, set, delete, ...
 * @param args variant containing the UCI message
 * @param commit UCI commit will be done if set to true
 */
int uci_call(const char* method, amxc_var_t* args, amxc_var_t* ret, bool commit) {
    int rv = 0;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("uci.");

    rv = amxb_call(ctx, "uci.", method, args, ret, 3);
    when_failed_trace(rv, exit, ERROR, "failed to call UCI '%s', function returned %d", method, rv);

    if(commit) {
        uci_commit_delayed();
    }

exit:
    return rv;
}

int mask_to_prefix_length(const char* mask, bool mandatory) {
    int i;
    bool breakout = false;
    int prefixlen = 0;
    unsigned char buffer[4];

    if(mandatory) {
        when_str_empty_trace(mask, exit, ERROR, "Failed to convert, subnetmask can not be empty");
    } else {
        when_str_empty_trace(mask, exit, INFO, "No subnetmask provided");
    }

    if(inet_pton(AF_INET, mask, buffer) != 1) {
        SAH_TRACEZ_ERROR(ME, "Failed to convert subnetmask");
        goto exit;
    }

    for(i = 0; i < 4 && !breakout; i++) {
        switch(buffer[i]) {
        case 0x00:
            breakout = true;
            break;
        case 0x80:
            prefixlen += 1;
            breakout = true;
            break;
        case 0xC0:
            prefixlen += 2;
            breakout = true;
            break;
        case 0xE0:
            prefixlen += 3;
            breakout = true;
            break;
        case 0xF0:
            prefixlen += 4;
            breakout = true;
            break;
        case 0xF8:
            prefixlen += 5;
            breakout = true;
            break;
        case 0xFC:
            prefixlen += 6;
            breakout = true;
            break;
        case 0xFE:
            prefixlen += 7;
            breakout = true;
            break;
        case 0xFF:
            prefixlen += 8;
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Failed to convert subnetmask '%s', invalid value", mask);
            prefixlen = 0;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_INFO(ME, "Converted subnetmask '%s' into prefix length '%d'", mask, prefixlen);
    return prefixlen;
}

static void form_old_address(amxc_string_t* full_old_addr, amxc_var_t* params,
                             int prefixlen, int old_prefixlen) {
    const char* ipaddress = GETP_CHAR(params, "parameters.IPAddress");
    const char* old_ipaddress = GET_CHAR(params, "OldIPAddress");

    amxc_string_init(full_old_addr, 0);

    if(((old_ipaddress != NULL) && (*old_ipaddress != '\0'))) {
        if(old_prefixlen <= 0) {
            old_prefixlen = prefixlen;
        }
        when_failed_trace(amxc_string_setf(full_old_addr, "%s/%d", old_ipaddress, old_prefixlen),
                          exit, ERROR, "Failed combine IP address and prefix");
    } else if(old_prefixlen > 0) {
        when_failed_trace(amxc_string_setf(full_old_addr, "%s/%d", ipaddress, old_prefixlen),
                          exit, ERROR, "Failed combine IP address and prefix");
    }
exit:
    return;
}

static int uci_set_ipaddr_list(amxc_var_t* uci_args, amxc_var_t* ipaddr_list,
                               amxc_var_t* values, const char* option) {
    int rv = -1;
    amxc_var_t* uci_ret = NULL;

    // when ipaddr_list is NULL, nothing changed and the list does not need to be set
    when_null_status(ipaddr_list, exit, rv = 0);
    if(GETP_ARG(ipaddr_list, "0.0.0") == NULL) {
        amxc_var_add_key(cstring_t, values, option, "");
    } else {
        amxc_var_add_key(amxc_llist_t, values, option,
                         amxc_var_constcast(amxc_llist_t, GETP_ARG(ipaddr_list, "0.0")));
    }

    rv = uci_call("set", uci_args, uci_ret, true);
    when_failed_trace(rv, exit, ERROR, "uci.set failed");

exit:
    amxc_var_delete(&uci_ret);
    return rv;
}

int uci_set_ip_address(amxc_var_t* params, ip_action_t ip_action,
                       int prefixlen, int old_prefixlen, const char* option) {
    int rv = -1;
    amxc_string_t full_addr;
    amxc_var_t* ipaddr_list = NULL;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;

    const char* ipaddress = GETP_CHAR(params, "parameters.IPAddress");
    const char* section_name = GET_CHAR(params, "UCIsectionName");

    amxc_string_init(&full_addr, 0);
    when_failed(init_uci_args(section_name, &uci_args), exit);
    when_false(prefixlen > 0, exit);
    when_str_empty(option, exit);
    when_str_empty_trace(ipaddress, exit, ERROR, "No IP address provided");
    when_failed_trace(amxc_string_setf(&full_addr, "%s/%d", ipaddress, prefixlen),
                      exit, ERROR, "Failed combine IP address and prefix");

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "proto", "static");

    switch(ip_action) {
    case IP_ADD: {
        amxc_string_t full_old_addr;
        form_old_address(&full_old_addr, params, prefixlen, old_prefixlen);
        ipaddr_list = list_replace_ipaddrs(section_name, amxc_string_get(&full_addr, 0),
                                           amxc_string_get(&full_old_addr, 0), option);
        amxc_string_clean(&full_old_addr);
        break;
    }
    case IP_DELETE: {
        ipaddr_list = list_delete_ipaddr(section_name, amxc_string_get(&full_addr, 0), option);
        break;
    }
    default:
        goto exit;
    }

    rv = uci_set_ipaddr_list(&uci_args, ipaddr_list, values, option);

exit:
    amxc_string_clean(&full_addr);
    amxc_var_delete(&ipaddr_list);
    amxc_var_clean(&uci_args);
    return rv;
}
