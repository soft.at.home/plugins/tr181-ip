/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ipv4_addresses.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"
#include "ip_manager_interface.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "mock_internal_functions.h"
#include "test_handling.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {

    common_setup(state, false, false);
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

void test_handle_new_netdevname_missing_params(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.wan.");
    ip_intf_info_t* intf = NULL;

    assert_non_null(intf_obj);
    assert_non_null(intf_obj->priv);
    intf = (ip_intf_info_t*) intf_obj->priv;

    assert_null(intf->intf_name);
    assert_null(intf->nm_intf_path);

    handle_new_netdevname(NULL, NULL);
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_UNKNOWN), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, ""), 0);
    handle_new_netdevname(NULL, "eth0");
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_UNKNOWN), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, ""), 0);

    // Providing an intf but no name should update the status to down
    handle_new_netdevname(intf, NULL);
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_DOWN), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, ""), 0);
}

/*
 * Helper function to expect a call to apply (if apply=true) or disable (apply=false)
 * for all instances of IPv4Address, IPv6Address and IPv6Prefix in the mock_defaults.odl
 */
static void expect_functions(bool apply) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj_tmpl = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.");
    amxd_object_t* addrv6_obj_tmpl = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.");
    amxd_object_t* prefix_obj_tmpl = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.");
    int addr_instance_count = amxd_object_get_instance_count(addr_obj_tmpl);
    int addrv6_instance_count = amxd_object_get_instance_count(addrv6_obj_tmpl);
    int prefix_instance_count = amxd_object_get_instance_count(prefix_obj_tmpl);

    if(apply) {
        expect_function_calls(__wrap_ipv4_addr_apply, addr_instance_count);
        expect_function_calls(__wrap_ipv6_prefix_apply, prefix_instance_count);
        expect_function_calls(__wrap_ipv6_addr_apply, addrv6_instance_count);
    } else {
        expect_function_calls(__wrap_ipv4_addr_disable, addr_instance_count);
        expect_function_calls(__wrap_ipv6_addr_disable, addrv6_instance_count);
        expect_function_calls(__wrap_ipv6_prefix_disable, prefix_instance_count);
    }
}

void test_handle_new_netdevname_initial_name(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.wan.");
    ip_intf_info_t* intf = NULL;

    assert_non_null(intf_obj);
    assert_non_null(intf_obj->priv);
    intf = (ip_intf_info_t*) intf_obj->priv;

    assert_null(intf->intf_name);
    assert_null(intf->nm_intf_path);

    expect_functions(true);
    handle_new_netdevname(intf, "eth0");
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_UP), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, "eth0"), 0);

    assert_non_null(intf->intf_name);
    assert_string_equal(intf->intf_name, "eth0");
}

void test_handle_new_netdevname_name_change(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.wan.");
    ip_intf_info_t* intf = NULL;
    amxd_object_t* addr_obj_tmpl = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.");
    int addr_instance_count = amxd_object_get_instance_count(addr_obj_tmpl);

    assert_non_null(intf_obj);
    assert_non_null(intf_obj->priv);
    intf = (ip_intf_info_t*) intf_obj->priv;

    assert_non_null(intf->intf_name);
    assert_string_equal(intf->intf_name, "eth0");

    // Calling handle_new_netdevname with a new name should update the name in the DM and the info structure
    expect_functions(false);
    expect_functions(true);
    handle_new_netdevname(intf, "eth1");
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_UP), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, "eth1"), 0);

    assert_non_null(intf->intf_name);
    assert_string_equal(intf->intf_name, "eth1");
}

void test_handle_new_netdevname_same_name(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.wan.");
    ip_intf_info_t* intf = NULL;
    amxd_object_t* addr_obj_tmpl = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.");
    int addr_instance_count = amxd_object_get_instance_count(addr_obj_tmpl);

    assert_non_null(intf_obj);
    assert_non_null(intf_obj->priv);
    intf = (ip_intf_info_t*) intf_obj->priv;

    assert_non_null(intf->intf_name);
    assert_string_equal(intf->intf_name, "eth1");
    assert_int_equal(check_param_equals("Name", intf_obj, "eth1"), 0);
    ip_manager_intf_set_status(intf->obj, STATUS_DOWN); // Force the status to down for this test, after the handle_new_netdevname is called, it should go back to up
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_DOWN), 0);

    // Calling handle_new_netdevname with the same name should not update the name info but it should set the status to up
    // No apply/disable function are expected to be called
    handle_new_netdevname(intf, "eth1");
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_UP), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, "eth1"), 0);

    assert_non_null(intf->intf_name);
    assert_string_equal(intf->intf_name, "eth1");
}

void test_handle_new_netdevname_name_clear(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.wan.");
    ip_intf_info_t* intf = NULL;
    amxd_object_t* addr_obj_tmpl = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.");
    int addr_instance_count = amxd_object_get_instance_count(addr_obj_tmpl);

    assert_non_null(intf_obj);
    assert_non_null(intf_obj->priv);
    intf = (ip_intf_info_t*) intf_obj->priv;

    assert_non_null(intf->intf_name);
    assert_string_equal(intf->intf_name, "eth1");

    // Expect all disable functions to be called
    expect_functions(false);
    handle_new_netdevname(intf, "");
    // After handling an empty name, the status should have gone down and the Name needs to be cleared in the DM and the info structure
    assert_int_equal(check_param_equals("Status", intf_obj, STATUS_DOWN), 0);
    assert_int_equal(check_param_equals("Name", intf_obj, ""), 0);

    assert_null(intf->intf_name);
    assert_null(intf->nm_intf_path);
}