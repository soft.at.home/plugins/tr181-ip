/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "test_ipv6.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_NETDEV_DEFINITION "../common/mock_netdev_dm/netdev_definitions.odl"
#define ODL_NETDEV_DEFAULTS "mock_netdev_defaults.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

#define str_empty(x) ((x == NULL) || (*x == '\0'))

int test_setup(void** state) {

    common_setup(state, true, true);
    // Two name queries will be opened for every interface (name_query and mtu_name_query), name should be in list twice
    set_query_getResult_value("lo lo eth0 eth0 br-lan br-lan");
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

void test_ipv6_ula_prefix_change(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* prefix_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.ULA64.");
    amxd_object_t* addr_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.ULA.");
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    assert_intf_name(dm, "IP.Interface.lan.", "br-lan");

    // For the inital values to work, a default MAC address should be set on the NetDev interface linked to the lan (mock_netdev_defaults.odl)

    // Before changing the ULA prefix, the ULA64 prefix should be a combination of the default parent prefix and the ChildPrefixBits in mock_defaults.odl
    assert_int_equal(check_param_equals("Prefix", prefix_obj, "fd01:2345:6789:1::/64"), 0);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, "fd01:2345:6789:1:b898:76ff:fe54:3210"), 0);

    // Changing the ULAPrefix should update the child prefix and in turn the address
    amxd_trans_select_pathf(&trans, "IP.");
    amxd_trans_set_value(cstring_t, &trans, "ULAPrefix", "fd98:7654:3210::/48");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(check_param_equals("Prefix", prefix_obj, "fd98:7654:3210:1::/64"), 0);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, "fd98:7654:3210:1:b898:76ff:fe54:3210"), 0);

    // Changing the ChildPrefixBits of the child prefix should update the prefix and in turn the address
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.ULA64.");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:5::/64");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(check_param_equals("Prefix", prefix_obj, "fd98:7654:3210:5::/64"), 0);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, "fd98:7654:3210:5:b898:76ff:fe54:3210"), 0);
}

void test_ipv6_parentprefix_prefix_changed(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* obj = NULL;
    amxd_object_t* netdev_obj = amxd_dm_findf(dm, "NetDev.Link.1.");
    amxd_object_t* lan_gua_prefix = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA.");
    amxd_object_t* lan_gua_address = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.GUA.");
    amxd_object_t* lan_iapd_prefix = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA_IAPD.");
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    // Set a known MAC address on the Link matching with the lan interface
    assert_non_null(netdev_obj);
    assert_int_equal(amxd_object_set_value(cstring_t, netdev_obj, "LLAddress", "01:23:45:67:89:AB"), 0);

    // Updating a prefix should update all the Prefixes and addresses that depend on it
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.GUA_IAPD.");
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "1234:abc::/48");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(check_param_equals("Prefix", lan_gua_prefix, "1234:abc::/64"), 0);
    assert_int_equal(check_param_equals("IPAddress", lan_gua_address, "1234:abc::1"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_iapd_prefix, "1234:abc:0:10::/60"), 0);
}

void test_ipv6_child_prefix_changed(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* obj = NULL;
    const char* old_prefix = NULL;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    // Make sure the parent prefix are a known value
    amxd_trans_select_pathf(&trans, "IP.");
    amxd_trans_set_value(cstring_t, &trans, "ULAPrefix", "fd01:2345:6789::/48");
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.GUA.");
    amxd_trans_set_value(cstring_t, &trans, "ParentPrefix", "Device.IP.Interface.2.IPv6Prefix.1.");
    amxd_trans_select_pathf(&trans, "IP.Interface.2.IPv6Prefix.1.");
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "2021:abc::/48");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    obj = amxd_dm_findf(dm, "%s", "IP.Interface.wan.IPv6Prefix.GUA_RA.");
    old_prefix = GET_CHAR(amxd_object_get_param_value(obj, "Prefix"), NULL);
    assert_non_null(old_prefix);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.ULA64.");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:A::/64");
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.GUA.");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:B::/64");
    //GUA_RA should not react to changes on ChildPrefixBits
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.GUA_RA.");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:C::/64");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    amxut_bus_handle_events();

    // Verify that the prefix is unchanged
    assert_int_equal(check_param_equals("Prefix", obj, old_prefix), 0);

    obj = amxd_dm_findf(dm, "%s", "IP.Interface.lan.IPv6Prefix.ULA64.");
    assert_int_equal(check_param_equals("Prefix", obj, "fd01:2345:6789:a::/64"), 0);

    obj = amxd_dm_findf(dm, "%s", "IP.Interface.lan.IPv6Prefix.GUA.");
    assert_int_equal(check_param_equals("Prefix", obj, "2021:abc:0:b::/64"), 0);

    amxd_trans_clean(&trans);
}

void test_ipv6_interface_id_changed(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_object_t* lan_gua_addr_obj = amxd_dm_findf(dm, "%s", "IP.Interface.lan.IPv6Address.GUA.");
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, lan_gua_addr_obj);
    amxd_trans_set_value(cstring_t, &trans, "InterfaceID", "::1234:5678:9ABC:0/128");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(check_param_equals("IPAddress", lan_gua_addr_obj, "2021:abc:0:b:1234:5678:9abc:0"), 0);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, lan_gua_addr_obj);
    amxd_trans_set_value(cstring_t, &trans, "InterfaceID", "::1/128");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(check_param_equals("IPAddress", lan_gua_addr_obj, "2021:abc:0:b::1"), 0);
}

void test_ipv6_dhcp_iapd(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* dhcp_iapd = NULL;
    amxd_object_t* gua_ra = NULL;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    gua_ra = amxd_dm_findf(dm, "%s", "IP.Interface.wan.IPv6Address.GUA_RA.");
    assert_non_null(gua_ra);
    assert_true(amxd_object_get_value(bool, gua_ra, "Enable", NULL));

    amxd_trans_select_object(&trans, gua_ra);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    dhcp_iapd = amxd_dm_findf(dm, "%s", "IP.Interface.wan.IPv6Address.DHCP_IAPD.");
    assert_non_null(dhcp_iapd);
    assert_true(amxd_object_get_value(bool, dhcp_iapd, "Enable", NULL));

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, gua_ra);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    assert_false(amxd_object_get_value(bool, dhcp_iapd, "Enable", NULL));
}

void test_wrong_reasons(UNUSED void** state) {
    assert_int_equal(_ipv6_prefix_destroy(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_ipv6_addr_destroy(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
}

void test_ipv6_prefix_address_static_static_toggle(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* prefix_object = NULL;
    amxd_object_t* address_object = NULL;
    amxd_dm_t* dm = amxut_bus_dm();
    char* obj_path = NULL;
    char* prefix_str = NULL;
    char* address_str = NULL;
    char* prefix_status = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_static_static");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Static");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "Static");
    amxd_trans_set_value(bool, &trans, "Autonomous", true);
    amxd_trans_set_value(cstring_t, &trans, "PreferredLifetime", "9999-12-31T23:59:59Z");
    amxd_trans_set_value(cstring_t, &trans, "ValidLifetime", "9999-12-31T23:59:59Z");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    prefix_object = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.new_static_static.");
    obj_path = amxd_object_get_path(prefix_object, AMXD_OBJECT_INDEXED);

    prefix_status = amxd_object_get_value(cstring_t, prefix_object, "Status", NULL);
    assert_string_equal(prefix_status, "Error");
    free(prefix_status);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, prefix_object);
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "2a02:1802:94:1::/64");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    prefix_status = amxd_object_get_value(cstring_t, prefix_object, "Status", NULL);
    assert_string_equal(prefix_status, "Enabled");
    free(prefix_status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Address.");
    amxd_trans_add_inst(&trans, 0, "GUA_STATIC");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Origin", "AutoConfigured");
    amxd_trans_set_value(cstring_t, &trans, "PreferredLifetime", "9999-12-31T23:59:59Z");
    amxd_trans_set_value(cstring_t, &trans, "ValidLifetime", "9999-12-31T23:59:59Z");
    amxd_trans_set_value(cstring_t, &trans, "Prefix", obj_path);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    address_object = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.GUA_STATIC.");
    assert_non_null(address_object);

    address_str = amxd_object_get_value(cstring_t, address_object, "IPAddress", NULL);
    assert_false(str_empty(address_str));
    free(address_str);

    trans_set_enable("IP.Interface.lan.IPv6Prefix.new_static_static.", false);
    amxut_bus_handle_events();

    prefix_object = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.new_static_static.");

    prefix_str = amxd_object_get_value(cstring_t, prefix_object, "Prefix", NULL);
    assert_string_equal(prefix_str, "2a02:1802:94:1::/64");
    free(prefix_str);

    address_str = amxd_object_get_value(cstring_t, address_object, "IPAddress", NULL);
    assert_true(str_empty(address_str));
    free(address_str);
    free(obj_path);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, prefix_object);
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    prefix_status = amxd_object_get_value(cstring_t, prefix_object, "Status", NULL);
    assert_string_equal(prefix_status, "Disabled");
    free(prefix_status);
}
