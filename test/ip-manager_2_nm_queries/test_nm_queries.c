/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxut/amxut_bus.h>
#include <ipat/ipat.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_ip.h"
#include "ip_manager_interface.h"
#include "ip_manager_entrypoint.h"
#include "ip_manager_common.h"
#include "ipv4_addresses.h"
#include "ipv6_addresses.h"
#include "ipv6_prefixes.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "mock_internal_functions.h"
#include "test_nm_queries.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {

    common_setup(state, false, true);
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

static void check_addr_query(ipv4_addr_info_t* addr_info, netmodel_query_t* query, const char* flags, const char* name) {
    assert_non_null(addr_info);
    assert_non_null(query);
    assert_non_null(query->impl);
    assert_non_null(query->cb);

    char* path = amxd_object_get_path(addr_info->intf_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    assert_non_null(path);
    assert_non_null(query->impl->given_intf);
    assert_string_equal(query->impl->given_intf, path);
    assert_ptr_equal(query->cb->userdata, addr_info);
    assert_string_equal(query->impl->subscriber, "ip-mngr");
    assert_string_equal(GET_CHAR(&query->impl->arguments, "flag"), flags);
    assert_string_equal(GET_CHAR(&query->impl->arguments, "name"), name);
    assert_string_equal(GET_CHAR(&query->impl->arguments, "traverse"), "down");
    free(path);
}

/*
 * Calls the nm_open_dhcpv4_query with invalid inputs, should return -1
 * Makes a valid call to nm_open_dhcpv4_query and verifies the data in the resulting netmodel queries
 */
void test_nm_open_dhcpv4_query(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv4Address.dhcp_ipv4.");

    assert_int_equal(nm_open_dhcpv4_query(NULL), -1);
    assert_int_equal(nm_open_dhcpv4_query(addr_info), -1);

    assert_non_null(addr_obj);
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;
    assert_non_null(addr_info);

    assert_null(addr_info->nm_ip_query);
    assert_null(addr_info->nm_subnet_query);
    assert_int_equal(nm_open_dhcpv4_query(addr_info), 0);
    assert_non_null(addr_info->nm_ip_query);

    check_addr_query(addr_info, addr_info->nm_ip_query, "dhcpv4", "IPAddress");

    assert_non_null(addr_info->nm_subnet_query);
    assert_string_equal(addr_info->nm_subnet_query->impl->given_intf, "Device.IP.Interface.1.");
    assert_ptr_equal(addr_info->nm_subnet_query->cb->userdata, addr_info);
    assert_string_equal(addr_info->nm_subnet_query->impl->subscriber, "ip-mngr");
    assert_int_equal(GET_UINT32(&addr_info->nm_subnet_query->impl->arguments, "tag"), 1);
    assert_string_equal(GET_CHAR(&addr_info->nm_subnet_query->impl->arguments, "type"), "req");
    assert_string_equal(GET_CHAR(&addr_info->nm_subnet_query->impl->arguments, "traverse"), "this");
}

/*
 * Calls the nm_open_ipcpv4_query with invalid inputs, should return -1
 * Makes a valid call to nm_open_ipcpv4_query and verifies the data in the resulting netmodel query
 */
void test_nm_open_ipcpv4_query(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;

    assert_int_equal(nm_open_ipcpv4_query(NULL), -1);
    assert_int_equal(nm_open_ipcpv4_query(addr_info), -1);

    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv4Address.ipcp_ipv4.");

    assert_non_null(addr_obj);
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;
    assert_non_null(addr_info);

    assert_null(addr_info->nm_ip_query);
    assert_null(addr_info->nm_subnet_query);
    nm_open_ipcpv4_query(addr_info);
    assert_non_null(addr_info->nm_ip_query);
    check_addr_query(addr_info, addr_info->nm_ip_query, "ppp", "LocalIPAddress");

    assert_null(addr_info->nm_subnet_query);
}

/*
 * Calls the nm_ip_query and nm_subnet_query callback handlers
 * Checks if the information is set in the datamodel
 * These calls are made without a NetDev name set in the intf_info struct, netdev_name_known will fail
 * The subscribe_if_added_to_netdev function should not be called
 */
void test_nm_open_dhcpv4_query_cb_no_name(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv4Address.dhcp_ipv4.");
    const char* expected_ip = "192.168.1.1";
    const char* expected_subnet = "255.255.255.0";
    amxc_var_t args;
    amxc_var_init(&args);

    assert_non_null(addr_obj);
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;
    assert_non_null(addr_info);
    assert_non_null(addr_info->nm_ip_query);
    assert_non_null(addr_info->nm_subnet_query);

    // Verify the initial values
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, ""), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", expected_ip);
    amxc_var_add_key(cstring_t, &args, "SubnetMask", expected_subnet);

    // Call the nm_ip_query callback function
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, expected_ip), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, ""), 0);

    // Call the nm_subnet_query callback function
    addr_info->nm_subnet_query->cb->handler(NULL, GET_ARG(&args, "SubnetMask"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, expected_ip), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, expected_subnet), 0);

    amxc_var_clean(&args);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", "");
    amxc_var_add_key(cstring_t, &args, "SubnetMask", "");
    // Call the nm_ip_query callback function to clear the IP
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, expected_subnet), 0);

    // Call the nm_subnet_query callback function
    addr_info->nm_subnet_query->cb->handler(NULL, GET_ARG(&args, "SubnetMask"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, ""), 0);
    amxc_var_clean(&args);
}

/*
 * Calls the nm_ip_query callback handler
 * Checks if the information is set in the datamodel
 * This call is made without a NetDev name set in the intf_info struct, netdev_name_known will fail
 * The subscribe_if_added_to_netdev function should not be called
 */
void test_nm_open_ipcpv4_query_cb_no_name(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv4Address.ipcp_ipv4.");
    const char* expected_ip = "192.168.2.1";
    amxc_var_t args;
    amxc_var_init(&args);

    assert_non_null(addr_obj);
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;
    assert_non_null(addr_info);
    assert_non_null(addr_info->nm_ip_query);

    // Verify the initial values
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, "255.255.255.255"), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", expected_ip);

    // Call the nm_ip_query callback function
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, expected_ip), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, "255.255.255.255"), 0);

    amxc_var_clean(&args);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", "");

    // Call the nm_ip_query callback function to clear the IP
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, "255.255.255.255"), 0);

    amxc_var_clean(&args);
}

/*
 * Calls the nm_ip_query and nm_subnet_query callback handlers
 * Checks if the information is set in the datamodel
 * These calls are made WITH a NetDev name set in the intf_info struct, netdev_name_known will not fail
 * The subscribe_if_added_to_netdev function should be called
 * For this test to work subscribe_if_added_to_netdev should be mocked
 */
void test_nm_open_dhcpv4_query_cb(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv4Address.dhcp_ipv4.");
    const char* expected_ip = "192.168.1.1";
    const char* expected_subnet = "255.255.255.0";
    amxc_var_t args;
    amxc_var_init(&args);

    assert_non_null(addr_obj);
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;
    assert_non_null(addr_info);
    // Set the interface name so netdev_name_known will not fail
    ip_intf_info_t* intf_info = addr_info->intf_obj->priv;
    if(str_empty(intf_info->intf_name)) {
        intf_info->intf_name = strdup("eth0");
    }

    assert_non_null(addr_info->nm_ip_query);
    assert_non_null(addr_info->nm_subnet_query);

    // Verify the initial values
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, ""), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", expected_ip);
    amxc_var_add_key(cstring_t, &args, "SubnetMask", expected_subnet);

    // Call the nm_ip_query callback function
    expect_function_call(__wrap_subscribe_if_added_to_netdev);
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);

    // After calling the IPAddress callback, the IP Address should be set, subnet mask should still be empty
    assert_int_equal(check_param_equals("IPAddress", addr_obj, expected_ip), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, ""), 0);

    // Call the nm_subnet_query callback function
    expect_function_call(__wrap_subscribe_if_added_to_netdev);
    addr_info->nm_subnet_query->cb->handler(NULL, GET_ARG(&args, "SubnetMask"), addr_info);

    // After calling the subnet mask callback, both parameters should be set in the datamodel
    assert_int_equal(check_param_equals("IPAddress", addr_obj, expected_ip), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, expected_subnet), 0);

    amxc_var_clean(&args);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", "");
    amxc_var_add_key(cstring_t, &args, "SubnetMask", "");
    // Call the nm_ip_query callback function to clear the IP
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, expected_subnet), 0);

    // Call the nm_subnet_query callback function
    addr_info->nm_subnet_query->cb->handler(NULL, GET_ARG(&args, "SubnetMask"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, ""), 0);
    amxc_var_clean(&args);
}

/*
 * Calls the nm_ip_query callback handler
 * Checks if the information is set in the datamodel
 * This call is made WITH a NetDev name set in the intf_info struct, netdev_name_known will not fail
 * The subscribe_if_added_to_netdev function should be called
 * For this test to work subscribe_if_added_to_netdev should be mocked
 */
void test_nm_open_ipcpv4_query_cb(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv4Address.ipcp_ipv4.");
    const char* expected_ip = "192.168.2.1";
    amxc_var_t args;
    amxc_var_init(&args);

    assert_non_null(addr_obj);
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;
    assert_non_null(addr_info);
    // Set the interface name so netdev_name_known will not fail
    ip_intf_info_t* intf_info = addr_info->intf_obj->priv;
    if(str_empty(intf_info->intf_name)) {
        intf_info->intf_name = strdup("eth0");
    }
    assert_non_null(addr_info->nm_ip_query);

    // Verify the initial values
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, "255.255.255.255"), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", expected_ip);

    // Call the nm_ip_query callback function
    expect_function_call(__wrap_subscribe_if_added_to_netdev);
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, expected_ip), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, "255.255.255.255"), 0);

    amxc_var_clean(&args);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IP", "");

    // Call the nm_ip_query callback function to clear the IP
    addr_info->nm_ip_query->cb->handler(NULL, GET_ARG(&args, "IP"), addr_info);
    assert_int_equal(check_param_equals("IPAddress", addr_obj, ""), 0);
    assert_int_equal(check_param_equals("SubnetMask", addr_obj, "255.255.255.255"), 0);

    amxc_var_clean(&args);
}