/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxc/amxc_macros.h>
#include <amxut/amxut_bus.h>

#include "ipv4_addresses.h"
#include "ip_manager_interface.h"
#include "ip_manager_interface_ipaddress.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"
#include "ip_manager_entrypoint.h"

#include "mock_netmodel.h"
#include "common_functions.h"

#define ODL_NETDEV_DEFINITION "../common/mock_netdev_dm/netdev_definitions.odl"
#define ODL_NETDEV_DEFAULTS "mock_netdev_defaults.odl"
#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

static int _dummy_function(void) {
    return AMXB_STATUS_OK;
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "app_start",
                                            AMXO_FUNC(_app_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ip_interface_added",
                                            AMXO_FUNC(_ip_interface_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_is_empty_or_in",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_address_added",
                                            AMXO_FUNC(_ipv4_address_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_addr_destroy",
                                            AMXO_FUNC(_ipv4_addr_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_address_update",
                                            AMXO_FUNC(_ipv4_address_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "enable_toggle_interface",
                                            AMXO_FUNC(_enable_toggle_interface)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_address_enable_changed",
                                            AMXO_FUNC(_ipv4_address_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "interface_destroy",
                                            AMXO_FUNC(_interface_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "lastchange_read",
                                            AMXO_FUNC(_lastchange_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setIPv4Address",
                                            AMXO_FUNC(_setIPv4Address)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "deleteIPv4Address",
                                            AMXO_FUNC(_deleteIPv4Address)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "reset_read",
                                            AMXO_FUNC(_reset_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "reset_interface",
                                            AMXO_FUNC(_reset_interface)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Reset",
                                            AMXO_FUNC(_Reset)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv4",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv6",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv6_prefix",
                                            AMXO_FUNC(_dummy_function)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "set_max_mtu_size",
                                            AMXO_FUNC(_set_max_mtu_size)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "neigh_reachable_time_changed",
                                            AMXO_FUNC(_neigh_reachable_time_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ip_interface_lower_layers_changed",
                                            AMXO_FUNC(_ip_interface_lower_layers_changed)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_added",
                                            AMXO_FUNC(_ipv6_prefix_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_destroy",
                                            AMXO_FUNC(_ipv6_prefix_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_enable_changed",
                                            AMXO_FUNC(_ipv6_prefix_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_changed",
                                            AMXO_FUNC(_ipv6_prefix_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_child_prefix_changed",
                                            AMXO_FUNC(_ipv6_child_prefix_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_prefix_changed",
                                            AMXO_FUNC(_ipv6_address_prefix_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_removed",
                                            AMXO_FUNC(_ipv6_prefix_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_parent_prefix_changed",
                                            AMXO_FUNC(_ipv6_parent_prefix_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ula_prefix_changed",
                                            AMXO_FUNC(_ula_prefix_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_added",
                                            AMXO_FUNC(_ipv6_address_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_update",
                                            AMXO_FUNC(_ipv6_address_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_interface_id_changed",
                                            AMXO_FUNC(_ipv6_interface_id_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_addr_destroy",
                                            AMXO_FUNC(_ipv6_addr_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_interface_going_down",
                                            AMXO_FUNC(_ipv6_interface_going_down)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_going_down",
                                            AMXO_FUNC(_ipv6_going_down)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_toggle_all",
                                            AMXO_FUNC(_ipv4_toggle_all)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_toggle_interface",
                                            AMXO_FUNC(_ipv4_toggle_interface)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_toggle_interface",
                                            AMXO_FUNC(_ipv6_toggle_interface)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_toggle_all",
                                            AMXO_FUNC(_ipv6_toggle_all)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_enable_changed",
                                            AMXO_FUNC(_ipv6_address_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv4_addressingtype_changed",
                                            AMXO_FUNC(_ipv4_addressingtype_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_origin_changed",
                                            AMXO_FUNC(_ipv6_prefix_origin_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_static_type_changed",
                                            AMXO_FUNC(_ipv6_prefix_static_type_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_origin_changed",
                                            AMXO_FUNC(_ipv6_address_origin_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_dhcpv6_or_slaac_changed",
                                            AMXO_FUNC(_ipv6_address_dhcpv6_or_slaac_changed)), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_preferred_lt_changed",
                                            AMXO_FUNC(_ipv6_prefix_preferred_lt_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_prefix_valid_lt_changed",
                                            AMXO_FUNC(_ipv6_prefix_valid_lt_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_preferred_lt_changed",
                                            AMXO_FUNC(_ipv6_address_preferred_lt_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ipv6_address_valid_lt_changed",
                                            AMXO_FUNC(_ipv6_address_valid_lt_changed)), 0);
}

int common_setup(void** state, bool netdev, bool netmodel) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root = NULL;

    amxut_bus_setup(state);

    dm = amxut_bus_dm();
    assert_non_null(dm);
    parser = amxut_bus_parser();
    assert_non_null(parser);
    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    resolver_add_all_functions(parser);

    // Load ODL files
    if(netdev) {
        assert_int_equal(amxo_parser_parse_file(parser, ODL_NETDEV_DEFINITION, root), 0);
        assert_int_equal(amxo_parser_parse_file(parser, ODL_NETDEV_DEFAULTS, root), 0);
    }
    if(netmodel) {
        resolve_dummy_netmodel_functions(parser);
        assert_int_equal(amxo_parser_parse_file(parser, ODL_MOCK_NETMODEL, root), 0);
    }
    assert_int_equal(amxo_parser_parse_file(parser, ODL_MOCK_MAIN, root), 0); // A stripped down version of odl/ip-manager.odl
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFS, root), 0);

    // Call entrypoint
    assert_int_equal(_ip_manager_main(AMXO_START, dm, parser), 0);

    // Load the defaults while events are disabled
    amxp_sigmngr_enable(&dm->sigmngr, false);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFAULTS, root), 0);
    amxp_sigmngr_enable(&dm->sigmngr, true);

    // Emit the app:start event so the defaults get initialised
    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    return 0;
}

int common_teardown(void** state) {

    assert_int_equal(_ip_manager_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    mock_netmodel_cleanup();

    amxut_bus_teardown(state);

    return 0;
}

static void check_param_persistency(amxd_object_t* obj, const char* param_name, bool persistent) {
    amxd_param_t* param = amxd_object_get_param_def(obj, param_name);
    assert_non_null(param);
    assert_true(amxd_param_is_attr_set(param, amxd_pattr_persistent) == persistent);
    assert_true(amxd_param_has_flag(param, "upc") == persistent);
}

/*
 * Helper function to check IPv4 parameter persistency.
 * Makes sure the persistent parameters that should change, changed.
 * Also checks if the persistent parameters that should be left alone are still persistent.
 * If more parameters made persistent, they should be added here.
 */
void check_ipv4_param_persistency(amxd_object_t* addr_obj, bool persistent) {
    assert_non_null(addr_obj);
    check_param_persistency(addr_obj, "IPAddress", persistent);
    check_param_persistency(addr_obj, "SubnetMask", persistent);
    check_param_persistency(addr_obj, "Enable", true);
    check_param_persistency(addr_obj, "Alias", true);
    check_param_persistency(addr_obj, "AddressingType", true);
}

/*
 * Helper function to check IPv6 parameter persistency.
 * Makes sure the persistent parameters that should change, changed.
 * Also checks if the persistent parameters that should be left alone are still persistent.
 * If more parameters made persistent, they should be added here.
 *
 * For DHCP_IPAD the enable will be set by the plugin, this parameter should not be persistent.
 * If auto_enabled is true we expect the behavior of DHCP_IAPD (enable not persistent)
 */
void check_ipv6_param_persistency(amxd_object_t* addr_obj, bool persistent, bool auto_enable) {
    assert_non_null(addr_obj);
    check_param_persistency(addr_obj, "Enable", !auto_enable);
    check_param_persistency(addr_obj, "IPAddress", persistent);
    check_param_persistency(addr_obj, "PreferredLifetime", persistent);
    check_param_persistency(addr_obj, "ValidLifetime", persistent);
    check_param_persistency(addr_obj, "RelativeValidLifetime", persistent);
    check_param_persistency(addr_obj, "RelativePreferredLifetime", persistent);
    check_param_persistency(addr_obj, "Alias", true);
    check_param_persistency(addr_obj, "Origin", true);
    check_param_persistency(addr_obj, "Prefix", true);
}

/*
 * Helper function to test persistency of auto detected IPv6Addresses.
 * For auto detected instances (like lla) the persistency should be removed from the object.
 * All parameters should have the upc flag removed and made read-only
 */
void check_ipv6_autodetected_persistency(amxd_object_t* addr_obj) {
    // All parameters in an auto detected instance should be read only, and NOT upgrade persistent
    amxc_llist_for_each(it, (&addr_obj->parameters)) {
        amxd_param_t* param = amxc_llist_it_get_data(it, amxd_param_t, it);
        assert_true(amxd_param_is_attr_set(param, amxd_pattr_read_only));
        assert_false(amxd_param_has_flag(param, "upc"));
    }
    // The whole address instance should not be persistent
    assert_false(amxd_object_is_attr_set(addr_obj, amxd_oattr_persistent));
}

/*
 * Helper function to test persistency of IPv6Prefixes.
 * Makes sure the persistent parameters that should change, changed.
 * Also checks if the persistent parameters that should be left alone are still persistent.
 * If more parameters made persistent, they should be added here.
 */
void check_ipv6_prefix_param_persistency(amxd_object_t* obj, bool persistent) {
    assert_non_null(obj);
    check_param_persistency(obj, "Prefix", persistent);
    check_param_persistency(obj, "PreferredLifetime", persistent);
    check_param_persistency(obj, "ValidLifetime", persistent);
    check_param_persistency(obj, "RelativeValidLifetime", persistent);
    check_param_persistency(obj, "RelativePreferredLifetime", persistent);
    check_param_persistency(obj, "Enable", true);
    check_param_persistency(obj, "Alias", true);
    check_param_persistency(obj, "Origin", true);
    check_param_persistency(obj, "StaticType", true);
}

int check_param_equals(const char* param_name, amxd_object_t* obj, const char* expected_value) {
    int rv = -1;
    const char* value = GET_CHAR(amxd_object_get_param_value(obj, param_name), NULL);
    assert_non_null(value);
    assert_non_null(expected_value);
    rv = strcmp(value, expected_value);
    if(rv != 0) {
        printf("'%s' != '%s'\n", value, expected_value);
        fflush(stdout);
    }

    return rv;
}

void assert_intf_name(amxd_dm_t* dm, const char* path, const char* assert_name) {
    ip_intf_info_t* intf = NULL;
    amxd_object_t* obj = amxd_dm_findf(dm, path);
    char* name = NULL;
    assert_non_null(obj);
    assert_non_null(obj->priv);

    intf = (ip_intf_info_t*) obj->priv;
    assert_non_null(intf->obj);
    if(*assert_name != '\0') {
        assert_non_null(intf->intf_name);
        assert_string_equal(intf->intf_name, assert_name);
    } else {
        assert_null(intf->intf_name);
    }
    assert_int_equal(check_param_equals("Name", obj, assert_name), 0);
}

void trans_set_enable(const char* path, bool enable) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, path);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
}