/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MOCK_INTERNAL_H__
#define __MOCK_INTERNAL_H__

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_be.h>
#include <amxb/amxb_connect.h>

int __wrap_cfgctrlr_execute_function(amxd_object_t* obj, const char* const func_name, amxc_var_t* args, amxc_var_t* ret);
int __wrap_nm_open_dhcpv4_query(ipv4_addr_info_t* addr_info);
int __wrap_nm_open_ipcpv4_query(ipv4_addr_info_t* addr_info);
int __wrap_update_ipv4_address(ipv4_addr_info_t* addr_info);
int __wrap_update_ipv6_address(ipv6_addr_info_t* addr_info);
int __wrap_nm_open_dhcpoption_query(amxd_object_t* intf_obj,
                                    netmodel_query_t** query,
                                    const char* type,
                                    int option,
                                    netmodel_callback_t nm_cb,
                                    void* userdata);
void __wrap_start_listening_for_changes(const char* prefix_path, const char* parameter, amxp_slot_fn_t fn, void* const priv);
int __wrap_ipv6_generate_and_set_in_dm(ipv6_addr_info_t* addr_info, const char* prefix_path);
int __wrap_ipv6_prefix_autoconfigured(ipv6_prefix_info_t* prefix_info);
int __wrap_ipv6_prefix_child(ipv6_prefix_info_t* prefix_info);
bool __wrap_netdev_name_known(amxd_object_t* intf_obj);
int __wrap_ipv4_addr_apply(ipv4_addr_info_t* addr_info);
int __wrap_ipv4_addr_disable(ipv4_addr_info_t* addr_info);
int __wrap_ipv6_addr_apply(ipv6_addr_info_t* addr_info);
int __wrap_ipv6_addr_disable(ipv6_addr_info_t* addr_info);
int __wrap_ipv6_prefix_apply(ipv6_prefix_info_t* prefix_info);
int __wrap_ipv6_prefix_disable(ipv6_prefix_info_t* prefix_info);
int __wrap_subscribe_if_added_to_netdev(amxd_object_t* intf_obj, amxd_object_t* addr_obj, const char* ip_addr, uint8_t ip_family);
int __wrap_dm_ipv6_prefix_set(const amxc_string_t* prefix, ipv6_prefix_info_t* prefix_info, const amxc_ts_t* preferred_lt, const amxc_ts_t* valid_lt, uint32_t plt, uint32_t vlt);

#endif // __MOCK_INTERNAL_H__

