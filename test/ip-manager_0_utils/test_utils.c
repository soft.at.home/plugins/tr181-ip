/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <ipat/ipat.h>

#include <amxut/amxut_bus.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>


#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_interface.h"
#include "ip_manager_entrypoint.h"
#include "ipv4_addresses.h"
#include "ipv6_addresses.h"
#include "ipv6_prefixes.h"

#include "../common/common_functions.h"
#include "test_utils.h"

#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"
#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root = NULL;

    amxut_bus_setup(state);

    dm = amxut_bus_dm();
    assert_non_null(dm);
    parser = amxut_bus_parser();
    assert_non_null(parser);
    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    resolver_add_all_functions(parser);

    // Load ODL files
    assert_int_equal(amxo_parser_parse_file(parser, ODL_MOCK_MAIN, root), 0); // A stripped down version of odl/ip-manager.odl
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFS, root), 0);

    // Load the defaults while events are disabled
    amxp_sigmngr_enable(&dm->sigmngr, false);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFAULTS, root), 0);
    amxp_sigmngr_enable(&dm->sigmngr, true);

    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    amxut_bus_teardown(state);

    return 0;
}

static int check_ipv6_child_parent_combine(const char* child, const char* parent, bool cidr, int expected_rv, const char* expected_combined_value) {
    int rv = -1;
    int function_rv = -1;
    amxc_string_t combined;
    amxc_string_init(&combined, 0);

    function_rv = ipv6_child_parent_combine(NULL, child, parent, &combined, cidr);
    if(function_rv != expected_rv) {
        rv = 1;
        printf("%d != %d\n", function_rv, expected_rv);
        fflush(stdout);
        goto exit;
    }

    if(amxc_string_get(&combined, 0) == NULL) {
        rv = 2;
        printf("returned null\n");
        fflush(stdout);
        goto exit;
    }

    if(strcmp(amxc_string_get(&combined, 0), expected_combined_value) != 0) {
        rv = 3;
        printf("%s != %s\n", amxc_string_get(&combined, 0), expected_combined_value);
        fflush(stdout);
        goto exit;
    }

    rv = 0;

exit:
    amxc_string_clean(&combined);
    return rv;
}

/*
 * Make sure the correct interface type is returned depending on the lowerlayer path that is provided
 * Default should be Ethernet
 */
void test_intf_get_lowerlayer_type(UNUSED void** state) {
    assert_int_equal(intf_get_lowerlayer_type(NULL), INTF_TYPE_ETHERNET);
    assert_int_equal(intf_get_lowerlayer_type(""), INTF_TYPE_ETHERNET);
    assert_int_equal(intf_get_lowerlayer_type("NULL"), INTF_TYPE_ETHERNET);
    assert_int_equal(intf_get_lowerlayer_type("Device.Ethernet.Interface.1"), INTF_TYPE_ETHERNET);
    assert_int_equal(intf_get_lowerlayer_type("NULL"), INTF_TYPE_ETHERNET);
    assert_int_equal(intf_get_lowerlayer_type("Device.Ethernet.VLANTermination.1"), INTF_TYPE_VLAN);
    assert_int_equal(intf_get_lowerlayer_type("Device.PPP.Interface.1"), INTF_TYPE_PPP);
}

/*
 * Make sure the correct interface type string is returned depending on the lowerlayer path that is provided
 * The returned value should match the flags used for the different types in NetModel
 * Default should be "eth", VLANTermination should be vlan and PPP should be ppp.
 */
void test_intf_get_lowerlayer_type_str(UNUSED void** state) {
    assert_string_equal(intf_get_lowerlayer_type_str(NULL), "eth");
    assert_string_equal(intf_get_lowerlayer_type_str(""), "eth");
    assert_string_equal(intf_get_lowerlayer_type_str("NULL"), "eth");
    assert_string_equal(intf_get_lowerlayer_type_str("Device.Ethernet.Interface.1"), "eth");
    assert_string_equal(intf_get_lowerlayer_type_str("NULL"), "eth");
    assert_string_equal(intf_get_lowerlayer_type_str("Device.Ethernet.VLANTermination.1"), "vlan");
    assert_string_equal(intf_get_lowerlayer_type_str("Device.PPP.Interface.1"), "ppp");
}

/*
 * Tests the creation/destruction of an interface structure
 * Tests the input parameter validation on the creation function, intf parameter should not be allowed to be NULL
 * Tests the input parameter validation on the clean function, providing NULL should not cause a problem
 * Tests if the info in structure is correct
 * Tests if it gets cleaned up properly
 */
void test_ip_intf_info_new(UNUSED void** state) {
    amxd_object_t* intf_obj = NULL;
    ip_intf_info_t* intf_info = NULL;

    assert_int_equal(ip_intf_info_new(NULL, NULL), -1);
    assert_int_equal(ip_intf_info_new(NULL, intf_obj), -1);
    assert_int_equal(ip_intf_info_new(&intf_info, NULL), -1);
    assert_null(intf_info);
    assert_int_equal(ip_intf_info_clean(NULL), amxd_status_ok);
    assert_int_equal(ip_intf_info_clean(&intf_info), amxd_status_ok);

    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    assert_null(intf_obj->priv);
    assert_int_equal(ip_intf_info_new(&intf_info, intf_obj), 0);
    assert_non_null(intf_info);
    assert_non_null(intf_obj->priv);
    assert_ptr_equal(intf_info->obj, intf_obj);

    assert_null(intf_info->ndl_ipv4addr_subscr);
    assert_null(intf_info->ndl_ipv6addr_subscr);
    assert_null(intf_info->name_query);
    assert_null(intf_info->status_query);
    assert_null(intf_info->mtu_name_query);

    ip_intf_info_clean(&intf_info);
    assert_null(intf_obj->priv);
    assert_null(intf_info);
}
/*
 * Tests the IPv4 Addressing type conversion from string to enum
 */
void test_convert_to_ipv4_addr_type(UNUSED void** state) {
    assert_int_equal(convert_to_ipv4_addr_type(NULL), IPV4_ADDR_TYPE_UNKNOWN);
    assert_int_equal(convert_to_ipv4_addr_type(""), IPV4_ADDR_TYPE_UNKNOWN);
    assert_int_equal(convert_to_ipv4_addr_type("Invalid value"), IPV4_ADDR_TYPE_UNKNOWN);

    assert_int_equal(convert_to_ipv4_addr_type("DHCP"), IPV4_ADDR_TYPE_DHCP);
    assert_int_equal(convert_to_ipv4_addr_type("IKEv2"), IPV4_ADDR_TYPE_IKEV2);
    assert_int_equal(convert_to_ipv4_addr_type("AutoIP"), IPV4_ADDR_TYPE_AUTOIP);
    assert_int_equal(convert_to_ipv4_addr_type("IPCP"), IPV4_ADDR_TYPE_IPCP);
    assert_int_equal(convert_to_ipv4_addr_type("Static"), IPV4_ADDR_TYPE_STATIC);
}

/*
 * Tests the prefixlen to subnetmask conversion
 * Tests to make sure invalid prefixlen return NULL
 */
void test_ipv4_addr_get_netmask_from_prefixlen(UNUSED void** state) {
    char* subnetmask = NULL;

    subnetmask = ipv4_addr_get_netmask_from_prefixlen(0);
    assert_null(subnetmask);

    subnetmask = ipv4_addr_get_netmask_from_prefixlen(-1);
    assert_null(subnetmask);

    subnetmask = ipv4_addr_get_netmask_from_prefixlen(33);
    assert_null(subnetmask);

    subnetmask = ipv4_addr_get_netmask_from_prefixlen(24);
    assert_non_null(subnetmask);
    assert_string_equal(subnetmask, "255.255.255.0");
    free(subnetmask);

    subnetmask = ipv4_addr_get_netmask_from_prefixlen(20);
    assert_non_null(subnetmask);
    assert_string_equal(subnetmask, "255.255.240.0");
    free(subnetmask);
}

/*
 * Tests the creation/destruction of an ipv4_addr_info_t structure
 * Tests the input parameter validation on the creation function, both addr_info and addr_obj are not allowed to be NULL
 * Tests the input parameter validation on the clean function, providing NULL should not cause a problem
 * Tests if the info in the addr_info structure is correct for a static and dhcp instance
 * Tests if it gets cleaned up properly
 */
void test_ipv4_addr_info_new_clean(UNUSED void** state) {
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* intf_obj = NULL;
    amxd_object_t* addr_obj = NULL;

    // Test input parameters
    assert_int_equal(ipv4_addr_info_new(NULL, NULL), amxd_status_invalid_function_argument);
    assert_int_equal(ipv4_addr_info_new(NULL, addr_obj), amxd_status_invalid_function_argument);
    assert_int_equal(ipv4_addr_info_new(&addr_info, NULL), amxd_status_invalid_function_argument);
    assert_null(addr_info);
    assert_int_equal(ipv4_addr_info_clean(NULL), amxd_status_ok);
    assert_int_equal(ipv4_addr_info_clean(&addr_info), amxd_status_ok);

    // Test valid case for static address
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    addr_obj = amxd_object_findf(intf_obj, "IPv4Address.static_ipv4.");
    assert_non_null(addr_obj);
    assert_null(addr_obj->priv); // If failed, ipv4_addr_info_new already called for this addr
    assert_int_equal(ipv4_addr_info_new(&addr_info, addr_obj), amxd_status_ok);
    assert_non_null(addr_obj->priv);

    assert_ptr_equal(addr_info->addr_obj, addr_obj);
    assert_ptr_equal(addr_info->intf_obj, intf_obj);
    assert_int_equal(addr_info->addr_type, IPV4_ADDR_TYPE_STATIC);
    assert_null(addr_info->nm_ip_query);
    assert_null(addr_info->nm_subnet_query);
    assert_null(addr_info->ipv4_param_subscr);

    assert_int_equal(ipv4_addr_info_clean(&addr_info), amxd_status_ok);
    assert_null(addr_info);

    // Test valid case for DHCP address, addr_type should be different then the static case
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    addr_obj = amxd_object_findf(intf_obj, "IPv4Address.dhcp_ipv4.");
    assert_non_null(addr_obj);
    assert_null(addr_obj->priv); // If failed, ipv4_addr_info_new already called for this addr
    assert_int_equal(ipv4_addr_info_new(&addr_info, addr_obj), amxd_status_ok);
    assert_non_null(addr_obj->priv);

    assert_ptr_equal(addr_info->addr_obj, addr_obj);
    assert_ptr_equal(addr_info->intf_obj, intf_obj);
    assert_int_equal(addr_info->addr_type, IPV4_ADDR_TYPE_DHCP);
    assert_null(addr_info->nm_ip_query);
    assert_null(addr_info->nm_subnet_query);
    assert_null(addr_info->ipv4_param_subscr);

    assert_int_equal(ipv4_addr_info_clean(&addr_info), amxd_status_ok);
    assert_null(addr_obj->priv);
    assert_null(addr_info);
}

/*
 * Tests the IPv6 interface ID type conversion from string to enum
 */
void test_convert_to_intf_id(UNUSED void** state) {
    assert_int_equal(convert_to_intf_id(NULL), UNKNOWN_INTF_ID);
    assert_int_equal(convert_to_intf_id(""), UNKNOWN_INTF_ID);
    assert_int_equal(convert_to_intf_id("Invalid value"), UNKNOWN_INTF_ID);

    assert_int_equal(convert_to_intf_id("EUI64"), EUI64_INTF_ID);
    assert_int_equal(convert_to_intf_id("Fixed"), FIXED_INTF_ID);
    assert_int_equal(convert_to_intf_id("None"), NONE_INTF_ID);
}

/*
 * Tests the IPv6 origin conversion from string to enum
 */
void test_addr_origin_to_enum(UNUSED void** state) {
    assert_int_equal(addr_origin_to_enum(NULL), ADDR_ORIGIN_UNKNOWN);
    assert_int_equal(addr_origin_to_enum(""), ADDR_ORIGIN_UNKNOWN);
    assert_int_equal(addr_origin_to_enum("Invalid value"), ADDR_ORIGIN_UNKNOWN);

    assert_int_equal(addr_origin_to_enum("AutoConfigured"), ADDR_ORIGIN_AUTOCONFIGURED);
    assert_int_equal(addr_origin_to_enum("DHCPv6"), ADDR_ORIGIN_DHCPV6);
    assert_int_equal(addr_origin_to_enum("IKEv2"), ADDR_ORIGIN_IKEV2);
    assert_int_equal(addr_origin_to_enum("MAP"), ADDR_ORIGIN_MAP);
    assert_int_equal(addr_origin_to_enum("WellKnown"), ADDR_ORIGIN_WELLKNOWN);
    assert_int_equal(addr_origin_to_enum("Static"), ADDR_ORIGIN_STATIC);
}

/*
 * Tests the creation/destruction of a ipv6_addr_info_t structure
 * Tests the input parameter validation on the creation function, both addr_info and addr_obj are not allowed to be NULL
 * Tests the input parameter validation on the clean function, providing NULL should not cause a problem
 * Tests if the info in the addr_info structure is correct for a dhcp and wellknown (autodetected) instance
 * Tests if it gets cleaned up properly
 */
void test_ipv6_addr_info_new_clean(UNUSED void** state) {
    ipv6_addr_info_t* addr_info = NULL;
    amxd_object_t* intf_obj = NULL;
    amxd_object_t* addr_obj = NULL;

    // Test input parameters
    assert_int_equal(ipv6_addr_info_new(NULL, NULL, false), amxd_status_invalid_function_argument);
    assert_int_equal(ipv6_addr_info_new(NULL, NULL, true), amxd_status_invalid_function_argument);
    assert_int_equal(ipv6_addr_info_new(NULL, addr_obj, false), amxd_status_invalid_function_argument);
    assert_int_equal(ipv6_addr_info_new(NULL, addr_obj, true), amxd_status_invalid_function_argument);
    assert_int_equal(ipv6_addr_info_new(&addr_info, NULL, false), amxd_status_invalid_function_argument);
    assert_null(addr_info);
    assert_int_equal(ipv6_addr_info_new(&addr_info, NULL, true), amxd_status_invalid_function_argument);
    assert_null(addr_info);
    assert_int_equal(ipv6_addr_info_clean(NULL), amxd_status_ok);
    assert_int_equal(ipv6_addr_info_clean(&addr_info), amxd_status_ok);

    // Test valid case for dhcpv6 address
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    addr_obj = amxd_object_findf(intf_obj, "IPv6Address.dhcp_ipv6.");
    assert_non_null(addr_obj);
    assert_null(addr_obj->priv); // If failed, ipv6_addr_info_new already called for this addr
    assert_int_equal(ipv6_addr_info_new(&addr_info, addr_obj, false), amxd_status_ok);

    assert_ptr_equal(addr_info->addr_obj, addr_obj);
    assert_ptr_equal(addr_info->intf_obj, intf_obj);
    assert_int_equal(addr_info->intf_id_type, NONE_INTF_ID);
    assert_int_equal(addr_info->origin, ADDR_ORIGIN_DHCPV6);
    assert_null(addr_info->nm_query);
    assert_null(addr_info->ipv6_param_subscr);
    assert_non_null(addr_info->preferred_timer);
    assert_non_null(addr_info->valid_timer);
    assert_int_equal(amxp_timer_get_state(addr_info->preferred_timer), amxp_timer_off);
    assert_int_equal(amxp_timer_get_state(addr_info->valid_timer), amxp_timer_off);
    assert_false(addr_info->auto_detected);

    assert_int_equal(ipv6_addr_info_clean(&addr_info), amxd_status_ok);
    assert_null(addr_obj->priv);
    assert_null(addr_info);

    // Test valid case for autodetected address (autodetected should be set to true in ipv6_addr_info_new)
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    addr_obj = amxd_object_findf(intf_obj, "IPv6Address.autodetected.");
    assert_non_null(addr_obj);
    assert_null(addr_obj->priv); // If failed, ipv6_addr_info_new already called for this addr
    assert_int_equal(ipv6_addr_info_new(&addr_info, addr_obj, true), amxd_status_ok);

    assert_ptr_equal(addr_info->addr_obj, addr_obj);
    assert_ptr_equal(addr_info->intf_obj, intf_obj);
    assert_int_equal(addr_info->intf_id_type, EUI64_INTF_ID);
    assert_int_equal(addr_info->origin, ADDR_ORIGIN_WELLKNOWN);
    assert_null(addr_info->nm_query);
    assert_null(addr_info->ipv6_param_subscr);
    assert_non_null(addr_info->preferred_timer);
    assert_non_null(addr_info->valid_timer);
    assert_int_equal(amxp_timer_get_state(addr_info->preferred_timer), amxp_timer_off);
    assert_int_equal(amxp_timer_get_state(addr_info->valid_timer), amxp_timer_off);
    assert_true(addr_info->auto_detected);

    assert_int_equal(ipv6_addr_info_clean(&addr_info), amxd_status_ok);
    assert_null(addr_obj->priv);
    assert_null(addr_info);
}

void test_prefix_origin_to_enum(UNUSED void** state) {
    assert_int_equal(prefix_origin_to_enum(NULL), PREFIX_ORIGIN_UNKNOWN);
    assert_int_equal(prefix_origin_to_enum(""), PREFIX_ORIGIN_UNKNOWN);
    assert_int_equal(prefix_origin_to_enum("Invalid value"), PREFIX_ORIGIN_UNKNOWN);

    assert_int_equal(prefix_origin_to_enum("AutoConfigured"), PREFIX_ORIGIN_AUTOCONFIGURED);
    assert_int_equal(prefix_origin_to_enum("PrefixDelegation"), PREFIX_ORIGIN_PREFIXDELEGATION);
    assert_int_equal(prefix_origin_to_enum("RouterAdvertisement"), PREFIX_ORIGIN_ROUTERADVERTISEMENT);
    assert_int_equal(prefix_origin_to_enum("WellKnown"), PREFIX_ORIGIN_WELLKNOWN);
    assert_int_equal(prefix_origin_to_enum("Static"), PREFIX_ORIGIN_STATIC);
    assert_int_equal(prefix_origin_to_enum("Child"), PREFIX_ORIGIN_CHILD);
}

void test_prefix_static_type_to_enum(UNUSED void** state) {
    assert_int_equal(prefix_static_type_to_enum(NULL), PREFIX_STATIC_TYPE_UNKNOWN);
    assert_int_equal(prefix_static_type_to_enum(""), PREFIX_STATIC_TYPE_UNKNOWN);
    assert_int_equal(prefix_static_type_to_enum("Invalid value"), PREFIX_STATIC_TYPE_UNKNOWN);

    assert_int_equal(prefix_static_type_to_enum("Static"), PREFIX_STATIC_TYPE_STATIC);
    assert_int_equal(prefix_static_type_to_enum("Inapplicable"), PREFIX_STATIC_TYPE_INAPPLICABLE);
    assert_int_equal(prefix_static_type_to_enum("PrefixDelegation"), PREFIX_STATIC_TYPE_PREFIXDELEGATION);
    assert_int_equal(prefix_static_type_to_enum("Child"), PREFIX_STATIC_TYPE_CHILD);
}

/*
 * Tests the creation/destruction of a ipv6_prefix_info_t structure
 * Tests the input parameter validation on the creation function, both prefix_info and prefix_info are not allowed to be NULL
 * Tests the input parameter validation on the clean function, providing NULL should not cause a problem
 * Tests if the info in the prefix_info structure is correct for a multiple different instances
 * Tests if it gets cleaned up properly
 */
void test_ipv6_prefix_info_new_clean(UNUSED void** state) {
    ipv6_prefix_info_t* prefix_info = NULL;
    amxd_object_t* intf_obj = NULL;
    amxd_object_t* prefix_obj = NULL;

    // Test input parameters
    assert_int_equal(ipv6_prefix_info_new(NULL, NULL), amxd_status_invalid_function_argument);
    assert_int_equal(ipv6_prefix_info_new(NULL, prefix_obj), amxd_status_invalid_function_argument);
    assert_int_equal(ipv6_prefix_info_new(&prefix_info, NULL), amxd_status_invalid_function_argument);
    assert_null(prefix_info);
    assert_int_equal(ipv6_prefix_info_clean(NULL), amxd_status_ok);
    assert_int_equal(ipv6_prefix_info_clean(&prefix_info), amxd_status_ok);

    // Test valid case for IPv6 prefix
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    prefix_obj = amxd_object_findf(intf_obj, "IPv6Prefix.ia_pd.");
    assert_non_null(prefix_obj);
    assert_null(prefix_obj->priv); // If failed, ipv6_prefix_info_new already called for this addr
    assert_int_equal(ipv6_prefix_info_new(&prefix_info, prefix_obj), amxd_status_ok);

    assert_ptr_equal(prefix_info->prefix_obj, prefix_obj);
    assert_ptr_equal(prefix_info->intf_obj, intf_obj);
    assert_int_equal(prefix_info->prefix_type, PREFIX_STATIC_TYPE_INAPPLICABLE);
    assert_int_equal(prefix_info->prefix_origin, PREFIX_ORIGIN_PREFIXDELEGATION);
    assert_null(prefix_info->nm_query);
    assert_non_null(prefix_info->preferred_timer);
    assert_non_null(prefix_info->valid_timer);
    assert_int_equal(amxp_timer_get_state(prefix_info->preferred_timer), amxp_timer_off);
    assert_int_equal(amxp_timer_get_state(prefix_info->valid_timer), amxp_timer_off);

    assert_int_equal(ipv6_prefix_info_clean(&prefix_info), amxd_status_ok);
    assert_null(prefix_obj->priv);
    assert_null(prefix_info);

    // Test valid case for IPv6 child prefix
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.");
    assert_non_null(intf_obj);
    prefix_obj = amxd_object_findf(intf_obj, "IPv6Prefix.child.");
    assert_non_null(prefix_obj);
    assert_null(prefix_obj->priv); // If failed, ipv6_prefix_info_new already called for this addr
    assert_int_equal(ipv6_prefix_info_new(&prefix_info, prefix_obj), amxd_status_ok);

    assert_ptr_equal(prefix_info->prefix_obj, prefix_obj);
    assert_ptr_equal(prefix_info->intf_obj, intf_obj);
    assert_int_equal(prefix_info->prefix_type, PREFIX_STATIC_TYPE_INAPPLICABLE);
    assert_int_equal(prefix_info->prefix_origin, PREFIX_ORIGIN_CHILD);
    assert_null(prefix_info->nm_query);
    assert_non_null(prefix_info->preferred_timer);
    assert_non_null(prefix_info->valid_timer);
    assert_int_equal(amxp_timer_get_state(prefix_info->preferred_timer), amxp_timer_off);
    assert_int_equal(amxp_timer_get_state(prefix_info->valid_timer), amxp_timer_off);

    assert_int_equal(ipv6_prefix_info_clean(&prefix_info), amxd_status_ok);
    assert_null(prefix_obj->priv);
    assert_null(prefix_info);
}

/*
 * Test if ipv6_child_parent_combine has safe input parameters
 * Test if a child and parent prefix get combined properly
 * Test if result is returned in cidr notation
 */
void test_ipv6_child_parent_combine_cidr(UNUSED void** state) {

    assert_int_equal(check_ipv6_child_parent_combine(NULL, "fe01:0abc:d0fc::1:c000/56", true, -1, ""), 0);
    assert_int_equal(check_ipv6_child_parent_combine("fe01:0abc:d0fc::1:c000/56", NULL, true, -1, ""), 0);
    assert_int_equal(check_ipv6_child_parent_combine("", "fe01:0abc:d0fc::/64", true, -1, ""), 0);
    assert_int_equal(check_ipv6_child_parent_combine("fe01:0abc:d0fc::/56", "", true, -1, ""), 0);

    assert_int_equal(ipv6_child_parent_combine(NULL, "fe01:0abc:d0fc::1:c000/56", "fe01:0abc:d0fc::/64", NULL, true), -1); // check_ipv6_child_parent_combine can't check providing NULL as combined

    // child prefix is longer as parent
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/64", "fe01::/56", true, 0, "fe01:0:0:1::/64"), 0);

    // parent prefix has same length as child
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/64", "fe01::/64", true, 0, "fe01::/64"), 0);

    // parent prefix is longer as child
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/56", "fe01::/64", true, -1, ""), 0);

    // parent prefix is longer than its prefix
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/64", "fe01:ffff:ffff:ffff:ffff::/56", true, 0, "fe01:ffff:ffff:ff01::/64"), 0);

    // parent prefix is longer as child, child is longer then parent prefix
    assert_int_equal(check_ipv6_child_parent_combine("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff/56", "fe01::/64", true, -1, ""), 0);
}

/*
 * Test if ipv6_child_parent_combine has safe input parameters
 * Test if a child and parent prefix get combined properly
 * Test if result is returned without cidr notation
 */
void test_ipv6_child_parent_combine(UNUSED void** state) {

    assert_int_equal(check_ipv6_child_parent_combine(NULL, "fe01:0abc:d0fc::1:c000/56", false, -1, ""), 0);
    assert_int_equal(check_ipv6_child_parent_combine("fe01:0abc:d0fc::1:c000/56", NULL, false, -1, ""), 0);
    assert_int_equal(check_ipv6_child_parent_combine("", "fe01:0abc:d0fc::/64", false, -1, ""), 0);
    assert_int_equal(check_ipv6_child_parent_combine("fe01:0abc:d0fc::/56", "", false, -1, ""), 0);

    assert_int_equal(ipv6_child_parent_combine(NULL, "fe01:0abc:d0fc::1:c000/56", "fe01:0abc:d0fc::/64", NULL, false), -1); // check_ipv6_child_parent_combine can't check providing NULL as combined

    // child prefix is longer as parent
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/64", "fe01::/56", false, 0, "fe01:0:0:1::"), 0);

    // parent prefix has same length as child
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/64", "fe01::/64", false, 0, "fe01::"), 0);

    // parent prefix is longer as child
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/56", "fe01::/64", false, -1, ""), 0);

    // parent prefix is longer than its prefix
    assert_int_equal(check_ipv6_child_parent_combine("0:0:0:1::/64", "fe01:ffff:ffff:ffff:ffff::/56", false, 0, "fe01:ffff:ffff:ff01::"), 0);

    // parent prefix is longer as child, child is longer then parent prefix
    assert_int_equal(check_ipv6_child_parent_combine("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff/56", "fe01::/64", false, -1, ""), 0);
}

/*
 * Toggles the persistency on an IPv4Address instance and checks if the persistency is ok
 */
void test_ipv4_toggle_persistency(UNUSED void** state) {
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.1.IPv4Address.1.");

    ipv4_toggle_persistency(addr_obj, true);
    check_ipv4_param_persistency(addr_obj, true);

    ipv4_toggle_persistency(addr_obj, false);
    check_ipv4_param_persistency(addr_obj, false);

    ipv4_toggle_persistency(addr_obj, true);
    check_ipv4_param_persistency(addr_obj, true);
}

/*
 * Toggles the persistency on an IPv6Address instance and checks if the persistency is ok
 */
void test_ipv6_toggle_persistency(UNUSED void** state) {
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.1.IPv6Address.1.");

    ipv6_toggle_persistency(addr_obj, true);
    check_ipv6_param_persistency(addr_obj, true, false);

    ipv6_toggle_persistency(addr_obj, false);
    check_ipv6_param_persistency(addr_obj, false, false);

    ipv6_toggle_persistency(addr_obj, true);
    check_ipv6_param_persistency(addr_obj, true, false);
}

/*
 * Toggles the persistency for an IPv6Address instance with AutoDetected == true
 */
void test_ipv6_toggle_persistency_autodetected(UNUSED void** state) {
    amxd_object_t* addr_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Address.autodetected.");

    // Auto detected instances should never be persistent
    ipv6_toggle_persistency(addr_obj, true);
    check_ipv6_autodetected_persistency(addr_obj);

    ipv6_toggle_persistency(addr_obj, false);
    check_ipv6_autodetected_persistency(addr_obj);

    ipv6_toggle_persistency(addr_obj, true);
    check_ipv6_autodetected_persistency(addr_obj);
}

/*
 * Toggles the persistency for an IPv6Prefix instance
 */
void test_ipv6_prefix_toggle_persistency(UNUSED void** state) {
    amxd_object_t* prefix_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Prefix.ia_pd.");

    ipv6_prefix_toggle_persistency(prefix_obj, true);
    check_ipv6_prefix_param_persistency(prefix_obj, true);

    ipv6_prefix_toggle_persistency(prefix_obj, false);
    check_ipv6_prefix_param_persistency(prefix_obj, false);

    ipv6_prefix_toggle_persistency(prefix_obj, true);
    check_ipv6_prefix_param_persistency(prefix_obj, true);
}

/*
 * Checks which Origins are considered dynamic and which are static
 */
void test_is_dynamic_ip(UNUSED void** state) {
    assert_true(is_dynamic_ip("AutoConfigured"));
    assert_true(is_dynamic_ip("DHCPv6"));
    assert_true(is_dynamic_ip("IKEv2"));
    assert_true(is_dynamic_ip("MAP"));

    assert_false(is_dynamic_ip("WellKnown"));
    assert_false(is_dynamic_ip("Static"));
    assert_false(is_dynamic_ip(""));
    assert_false(is_dynamic_ip(NULL));
}

/*
 * Checks if the system uptime increases
 * Allow for a range of -1s and +1s for inaccuracies
 */
void test_get_system_uptime(UNUSED void** state) {
    int initial_uptime = get_system_uptime();
    sleep(5);
    assert_in_range(get_system_uptime(), initial_uptime + 4, initial_uptime + 6);
}
