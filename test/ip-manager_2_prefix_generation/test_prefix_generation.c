/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "test_prefix_generation.h"

int test_setup(void** state) {

    common_setup(state, false, true);
    // Two name queries will be opened for every interface (name_query and mtu_name_query), name should be in list twice
    set_query_getResult_value("eth0 eth0 br-lan br-lan br-guest br-guest");
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

static int set_parent_prefix(const char* prefix) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* parent_prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.GUA_IAPD."); // Fetch the parent prefix
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    assert_non_null(parent_prefix_obj);
    amxd_trans_select_object(&trans, parent_prefix_obj);
    amxd_trans_set_value(cstring_t, &trans, "Prefix", prefix);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    return check_param_equals("Prefix", parent_prefix_obj, prefix);
}

static int set_child_prefix_bits(amxd_object_t* prefix_obj, const char* child_prefix_bits) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    assert_non_null(prefix_obj);
    amxd_trans_select_object(&trans, prefix_obj);
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", child_prefix_bits);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    return check_param_equals("ChildPrefixBits", prefix_obj, child_prefix_bits);
}

static int set_enable(amxd_object_t* prefix_obj, bool enable) {
    int rv = -1;
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    assert_non_null(prefix_obj);
    amxd_trans_select_object(&trans, prefix_obj);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    rv = amxd_trans_apply(&trans, dm);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    return rv;
}

/**
 * In mock defaults a three interfaces are defined (wan, lan guest).
 * For all these interfaces a few prefixes are defined.
 * Wan DHCP_IAPD is the parent prefix for all the other prefixes.
 *
 * Changing the parent prefix should update the child prefixes accordingly.
 */
void test_prefix_generation_parent_changes(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* wan_dhcp_iapd_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.DHCP_IAPD.");
    amxd_object_t* lan_gua_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA.");
    amxd_object_t* lan_gua_iapd_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA_IAPD.");
    amxd_object_t* guest_gua_obj = amxd_dm_findf(dm, "IP.Interface.guest.IPv6Prefix.GUA.");
    amxd_object_t* guest_gua_iapd_obj = amxd_dm_findf(dm, "IP.Interface.guest.IPv6Prefix.GUA_IAPD.");

    assert_non_null(wan_dhcp_iapd_obj);
    assert_non_null(lan_gua_obj);
    assert_non_null(lan_gua_iapd_obj);
    assert_non_null(guest_gua_obj);
    assert_non_null(guest_gua_iapd_obj);

    assert_int_equal(set_parent_prefix("dead:beef:cafe:1100::/56"), 0); // Sets a prefix that should allow for all child prefixes to be generated
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:11ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:1101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:1110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:1102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:1120::/60"), 0);

    assert_int_equal(set_parent_prefix("dead:beef:cafe:1100::/64"), 0); // A parent prefix with length 64 should only generate a prefix on LAN GUA
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:1100::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, ""), 0);

    assert_int_equal(set_parent_prefix("dead:beef:cafe:1100::/63"), 0);                           // A parent prefix with length 64 should only generate a prefix on LAN GUA
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:1101::/64"), 0);   // Will use its child prefix because address is available
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:1100::/64"), 0); // Will take the only prefix that is left
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, ""), 0);

    assert_int_equal(set_parent_prefix("dead:beef:cafe:1100::/60"), 0); // A parent prefix with length 64 should only generate a prefix on LAN GUA
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:110f::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:1101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:1100::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:1102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, ""), 0); // This will result in the same prefix as lan_gua_iapd_obj, should not be set
}

/**
 * In mock defaults a three interfaces are defined (wan, lan guest).
 * For all these interfaces a few prefixes are defined.
 * Wan DHCP_IAPD is the parent prefix for all the other prefixes.
 *
 * Changing the ChildPrefixBits of a prefix should update the prefix (and possible clear a different one if overlapping and more important)
 */
void test_prefix_generation_child_prefix_bit_changes(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* wan_dhcp_iapd_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.DHCP_IAPD.");
    amxd_object_t* lan_gua_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA.");
    amxd_object_t* lan_gua_iapd_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA_IAPD.");
    amxd_object_t* guest_gua_obj = amxd_dm_findf(dm, "IP.Interface.guest.IPv6Prefix.GUA.");
    amxd_object_t* guest_gua_iapd_obj = amxd_dm_findf(dm, "IP.Interface.guest.IPv6Prefix.GUA_IAPD.");

    assert_non_null(wan_dhcp_iapd_obj);
    assert_non_null(lan_gua_obj);
    assert_non_null(lan_gua_iapd_obj);
    assert_non_null(guest_gua_obj);
    assert_non_null(guest_gua_iapd_obj);

    assert_int_equal(set_parent_prefix("dead:beef:cafe:2100::/56"), 0); // Sets a prefix that should allow for all child prefixes to be generated
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:21ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:2101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:2110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:2102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:2120::/60"), 0);

    assert_int_equal(set_child_prefix_bits(guest_gua_obj, "0:0:0:1::/64"), 0); // Set the same child prefix bits as lan_gua_obj
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:21ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:2101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:2110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:2120::/60"), 0);

    assert_int_equal(set_child_prefix_bits(guest_gua_obj, "0:0:0:2::/64"), 0); // Set back to its own value
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:21ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:2101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:2110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:2102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:2120::/60"), 0);

    assert_int_equal(set_child_prefix_bits(lan_gua_obj, "0:0:0:2::/64"), 0);                    // Set the same child prefix bits as guest_gua_obj
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:21ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:2102::/64"), 0); // Should be updated to the new value
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:2110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, ""), 0);                       // Should be cleared as lan gua takes priority
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:2120::/60"), 0);

    assert_int_equal(set_child_prefix_bits(lan_gua_obj, "0:0:0:1::/64"), 0);                    // Set back to original value
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:21ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:2101::/64"), 0); // Should be updated to the new value
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:2110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, ""), 0);                       // Does not need to be updated
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:2120::/60"), 0);
}

/**
 * In mock defaults a three interfaces are defined (wan, lan guest).
 * For all these interfaces a few prefixes are defined.
 * Wan DHCP_IAPD is the parent prefix for all the other prefixes.
 *
 * Changing the Enable of a prefix should update the prefix (and possible clear a different one if overlapping and more important)
 */
void test_prefix_generation_enable_changes(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* wan_dhcp_iapd_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.DHCP_IAPD.");
    amxd_object_t* lan_gua_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA.");
    amxd_object_t* lan_gua_iapd_obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.GUA_IAPD.");
    amxd_object_t* guest_gua_obj = amxd_dm_findf(dm, "IP.Interface.guest.IPv6Prefix.GUA.");
    amxd_object_t* guest_gua_iapd_obj = amxd_dm_findf(dm, "IP.Interface.guest.IPv6Prefix.GUA_IAPD.");

    assert_non_null(wan_dhcp_iapd_obj);
    assert_non_null(lan_gua_obj);
    assert_non_null(lan_gua_iapd_obj);
    assert_non_null(guest_gua_obj);
    assert_non_null(guest_gua_iapd_obj);

    assert_int_equal(set_parent_prefix("dead:beef:cafe:3100::/56"), 0); // Sets a prefix that should allow for all child prefixes to be generated
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:31ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:3101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:3110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:3102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:3120::/60"), 0);

    assert_int_equal(set_enable(lan_gua_obj, false), 0); // Disabling the prefix should clear the DM
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:31ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:3110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:3102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:3120::/60"), 0);

    assert_int_equal(set_child_prefix_bits(guest_gua_obj, "0:0:0:1::/64"), 0); // Set the same child prefix bits as lan_gua_obj (should be set as LAN GUA is disabled)
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:31ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, ""), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:3110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:3101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:3120::/60"), 0);

    assert_int_equal(set_enable(lan_gua_obj, true), 0);                                         // Enabling the prefix should clear the GUEST GUA instance and set the prefix in LAN GUA
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:31ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:3101::/64"), 0); // Should be set now
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:3110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, ""), 0);                       // Should be cleared as lan gua takes priority
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:3120::/60"), 0);

    assert_int_equal(set_child_prefix_bits(guest_gua_obj, "0:0:0:2::/64"), 0); // Set back to its own value
    assert_int_equal(check_param_equals("Prefix", wan_dhcp_iapd_obj, "dead:beef:cafe:31ff::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_obj, "dead:beef:cafe:3101::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", lan_gua_iapd_obj, "dead:beef:cafe:3110::/60"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_obj, "dead:beef:cafe:3102::/64"), 0);
    assert_int_equal(check_param_equals("Prefix", guest_gua_iapd_obj, "dead:beef:cafe:3120::/60"), 0);
}
