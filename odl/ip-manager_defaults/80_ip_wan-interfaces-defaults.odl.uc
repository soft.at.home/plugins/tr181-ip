%populate {
{# find out if we have at least one wan interface #}{% if (BDfn.getUpstreamInterfaceIndex() != "-1"): %}
    object IP.Interface.voip {
        parameter Enable = false;
        parameter IPv4Enable = true;
        parameter IPv6Enable = false;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter MaxMTUSize = 1500;
        parameter Type = "Normal";
        parameter Loopback = false;
        parameter LowerLayers = "Device.Ethernet.Link.2";
        parameter UpLink = true;
    }

    object IP.Interface.iptv {
        parameter Enable = false;
        parameter IPv4Enable = true;
        parameter IPv6Enable = false;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter MaxMTUSize = 1500;
        parameter Type = "Normal";
        parameter Loopback = false;
        parameter LowerLayers = "Device.Ethernet.Link.2";
        parameter UpLink = true;
    }

    object IP.Interface.mgmt {
        parameter Enable = false;
        parameter IPv4Enable = true;
        parameter IPv6Enable = false;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter MaxMTUSize = 1500;
        parameter Type = "Normal";
        parameter Loopback = false;
        parameter LowerLayers = "Device.Ethernet.Link.2";
        parameter UpLink = true;
    }

    object IP.Interface.voip.IPv4Address {
        instance add("primary") {
            parameter Enable = true;
            parameter AddressingType = "DHCP";
        }
    }

    object IP.Interface.iptv.IPv4Address {
        instance add("primary") {
            parameter Enable = true;
            parameter AddressingType = "DHCP";
        }
    }

    object IP.Interface.mgmt.IPv4Address {
        instance add("primary") {
            parameter Enable = true;
            parameter AddressingType = "DHCP";
        }
    }
{% endif; %}
}
