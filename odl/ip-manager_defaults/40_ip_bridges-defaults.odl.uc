{# find out how many wan interfaces we have #}{% let wan_interfaces=0; for (let Itf in BD.Interfaces) : if (Itf.Upstream == "true" || Itf.DefaultUpstream == "true") : %}{% wan_interfaces++ %}{% endif; endfor; %}
%populate {
{% let i=0 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
    object IP.Interface.{{lc(Bridge)}} {
        parameter Enable = true;
        parameter IPv4Enable = true;
        parameter IPv6Enable = true;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter MaxMTUSize = 1500;
        parameter Type = "Normal";
        parameter Loopback = false;
        parameter LowerLayers = "Device.Ethernet.Link.{{wan_interfaces + 1 + i}}"; {# #wan_interfases + loopback + next index #}
    }

    object IP.Interface.{{lc(Bridge)}}.IPv4Address {
        instance add({{lc(Bridge)}}) {
            parameter Enable = true {
                userflags %usersetting;
            }
{% if ( BD.Bridges[Bridge].IPv4Address ) : %}
            parameter IPAddress = "{{BD.Bridges[Bridge].IPv4Address}}" {
{% else; %}
            parameter IPAddress = "192.168.{{i}}.{% if (BDfn.hasAnyUpstream()) : %}1{% else %}254{% endif; %}" {
{% endif; %}
                userflags %usersetting;
            }
{% if ( BD.Bridges[Bridge].SubnetMask ) : %}
            parameter SubnetMask = "{{BD.Bridges[Bridge].SubnetMask}}" {
{% else; %}
            parameter SubnetMask = "255.255.255.0" {
{% endif; %}
                userflags %usersetting;
            }
            parameter AddressingType = "Static";
            parameter Controller = "mod-ipm-netlink";
        }
{% if ( wan_interfaces == 0 ) : %}
        instance add("{{lc(Bridge)}}-dhcp") {
            parameter Enable = true;
            parameter AddressingType = "DHCP";
            parameter Controller = "mod-ipm-netlink";
        }
{% endif; %}
        instance add("public-{{lc(Bridge)}}") {
            parameter Enable = false;
            parameter AddressingType = "Static";
            parameter Controller = "mod-ipm-netlink";
        }
    }

    object IP.Interface.{{lc(Bridge)}}.IPv6Prefix {
        instance add ("ULA64") {
            parameter Enable = false;
            parameter Origin = "AutoConfigured";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ChildPrefixBits = "0:0:0:{{i}}::/64";
            parameter StaticType = "Inapplicable";
        }

{% if ( wan_interfaces == 0 ) : %}
        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "RouterAdvertisement";
            parameter StaticType = "Inapplicable";
        }
{% else %}
        instance add ("GUA") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter OnLink = true;
            parameter ParentPrefix = "${ip_intf_wan}IPv6Prefix.1";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:{{i}}::/64";
        }

        instance add ("GUA_IAPD") {
            parameter Enable = true;
            parameter Origin = "Static";
            parameter Prefix = "";
            parameter ParentPrefix = "${ip_intf_wan}IPv6Prefix.1";
            parameter StaticType = "Child";
            parameter ChildPrefixBits = "0:0:0:{{i}}0::/60";
        }

        instance add ("GUA_STATIC") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter OnLink = true;
            parameter ParentPrefix = "${ip_intf_wan}IPv6Prefix.4";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:{{i}}::/64";
        }
{% endif %}
    }

    object IP.Interface.{{lc(Bridge)}}.IPv6Address {
        instance add("ULA") {
            parameter Enable = false;
            parameter Prefix = "${ip_intf_{{lc(Bridge)}}}IPv6Prefix.1";
            parameter Origin = "AutoConfigured";
        }

        instance add("GUA") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "${ip_intf_{{lc(Bridge)}}}IPv6Prefix.2";
            parameter Origin = "AutoConfigured";
{% if ( wan_interfaces != 0 ) : %}
            parameter InterfaceIDType = "Fixed";
{% endif %}
            parameter InterfaceID = "::1/128";
        }

        instance add("GUA_STATIC") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "${ip_intf_{{lc(Bridge)}}}IPv6Prefix.4";
            parameter Origin = "AutoConfigured";
{% if ( wan_interfaces != 0 ) : %}
            parameter InterfaceIDType = "Fixed";
{% endif %}
            parameter InterfaceID = "::1/128";
        }
    }
{% endfor %}
}
