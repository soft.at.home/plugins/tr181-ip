{% let has_upstream = (BDfn.getUpstreamInterfaceIndex() != "-1") %}
{% let has_failover = (BDfn.getFailoverInterfaceIndex() != "-1") %}
{% let i=1 %}
%config {
    %global ip_intf_lo = "Device.IP.Interface.1.";
{% if (has_upstream): %}
    %global ip_intf_wan = "Device.IP.Interface.2.";
	{% i++ %}
{% endif; %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
    %global ip_intf_{{lc(Bridge)}} = "Device.IP.Interface.{{ i }}.";
{% endfor; %}
{% if (has_upstream): %}
    %global ip_intf_wan6 = "Device.IP.Interface.{{ i+1 }}.";
    %global ip_intf_dslite-entry = "Device.IP.Interface.{{ i+2 }}.";
    %global ip_intf_dslite-exit = "Device.IP.Interface.{{ i+3 }}.";
    %global ip_intf_voip = "Device.IP.Interface.{{ i+4 }}.";
    %global ip_intf_iptv = "Device.IP.Interface.{{ i+5 }}.";
    %global ip_intf_mgmt = "Device.IP.Interface.{{ i+6 }}.";
{% endif; %}
{% if (has_failover): %}
    %global ip_intf_failover = "Device.IP.Interface.{{i+7}}.";
{% endif; %}
}
