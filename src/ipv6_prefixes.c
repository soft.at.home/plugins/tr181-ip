/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_interface.h"
#include "ip_manager_common.h"
#include "ip_manager_controller.h"

#include "ipv6_prefixes.h"

#define ME "ipv6"

int ipv6_prefix_toggle_persistency(amxd_object_t* obj, bool is_persistent) {
    SAH_TRACEZ_IN(ME);
    amxd_param_t* prefix = NULL;
    amxd_param_t* preferredlifetime = NULL;
    amxd_param_t* validlifetime = NULL;
    amxd_param_t* relativePreferredLifetime = NULL;
    amxd_param_t* relativeValidLifetime = NULL;
    int rv = -1;

    prefix = amxd_object_get_param_def(obj, "Prefix");
    preferredlifetime = amxd_object_get_param_def(obj, "PreferredLifetime");
    validlifetime = amxd_object_get_param_def(obj, "ValidLifetime");
    relativeValidLifetime = amxd_object_get_param_def(obj, "RelativeValidLifetime");
    relativePreferredLifetime = amxd_object_get_param_def(obj, "RelativePreferredLifetime");

    when_null_trace(prefix, exit, ERROR, "Prefix could not be found");
    when_null_trace(preferredlifetime, exit, ERROR, "PreferredLifetime could not be found");
    when_null_trace(validlifetime, exit, ERROR, "ValidLifetime could not be found");
    when_null_trace(relativeValidLifetime, exit, ERROR, "RelativeValidLifetime could not be found");
    when_null_trace(relativePreferredLifetime, exit, ERROR, "RelativePreferredLifetime could not be found");

    rv = amxd_param_set_attr(prefix, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(preferredlifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(validlifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(relativeValidLifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(relativePreferredLifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");

    if(is_persistent) {
        amxd_param_set_flag(prefix, "upc");
        amxd_param_set_flag(preferredlifetime, "upc");
        amxd_param_set_flag(validlifetime, "upc");
        amxd_param_set_flag(relativeValidLifetime, "upc");
        amxd_param_set_flag(relativePreferredLifetime, "upc");
    } else {
        amxd_param_unset_flag(prefix, "upc");
        amxd_param_unset_flag(preferredlifetime, "upc");
        amxd_param_unset_flag(validlifetime, "upc");
        amxd_param_unset_flag(relativeValidLifetime, "upc");
        amxd_param_unset_flag(relativePreferredLifetime, "upc");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void new_prefix(const char* prefix_param_name,
                       const amxc_var_t* const data,
                       void* const priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_info_t* prefix_info = NULL;
    ipv6_prefix_info_t* parent_prefix_info = NULL;
    amxd_object_t* parent_prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    const amxc_ts_t* preferred_lt = NULL;
    const amxc_ts_t* valid_lt = NULL;
    uint32_t vlt = 0;
    uint32_t plt = 0;
    const char* parent_prefix = NULL;
    char* child_prefix_bits = NULL;
    amxc_string_t combined;
    amxc_var_t params;
    bool parent_prefix_enabled = false;

    amxc_string_init(&combined, 0);
    amxc_var_init(&params);

    when_null_trace(parent_prefix_obj, exit, ERROR, "failed to get parent prefix object");
    when_null(priv, exit);
    when_str_empty(prefix_param_name, exit);
    prefix_info = (ipv6_prefix_info_t*) priv;
    parent_prefix_info = (ipv6_prefix_info_t*) parent_prefix_obj->priv;

    amxd_object_get_params(parent_prefix_obj, &params, amxd_dm_access_protected);
    parent_prefix = GET_CHAR(&params, prefix_param_name);
    parent_prefix_enabled = GET_BOOL(&params, "Enable");

    if((parent_prefix == NULL) || (*parent_prefix == '\0')) {
        SAH_TRACEZ_INFO(ME, "Parent prefix was cleared");
    } else if(!parent_prefix_enabled
              && (strcmp(prefix_param_name, "Prefix") == 0)
              && (parent_prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC)
              && (parent_prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC)) {
        SAH_TRACEZ_INFO(ME, "Parent prefix was disabled");
    } else {
        child_prefix_bits = amxd_object_get_value(cstring_t,
                                                  prefix_info->prefix_obj,
                                                  "ChildPrefixBits",
                                                  NULL);

        ipv6_child_parent_combine(prefix_info,
                                  child_prefix_bits,
                                  parent_prefix,
                                  &combined,
                                  true);      // In case of failure, still set combined in DM to clear value
    }

    preferred_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
    valid_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));
    vlt = GET_UINT32(&params, "RelativeValidLifetime");
    plt = GET_UINT32(&params, "RelativePreferredLifetime");
    dm_ipv6_prefix_set(&combined, prefix_info, preferred_lt, valid_lt, plt, vlt);

exit:
    free(child_prefix_bits);
    amxc_string_clean(&combined);
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void nm_option25_cb(UNUSED const char* sig_name,
                           const amxc_var_t* result,
                           void* userdata) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_prefix_info_t* prefix_info = NULL;
    amxc_var_t* option = NULL;
    amxc_ts_t preferred_lt;
    amxc_ts_t valid_lt;
    amxc_string_t full_prefix;

    amxc_string_init(&full_prefix, 0);

    option = GETP_ARG(result, "0.Options.0");
    when_null_trace(userdata, exit, ERROR, "No userdata provided");
    prefix_info = (ipv6_prefix_info_t*) userdata;

    SAH_TRACEZ_INFO(ME, "Received an update to prefix '%s'",
                    amxd_object_get_name(prefix_info->prefix_obj, AMXD_OBJECT_NAMED));

    if(option == NULL) {
        rv = dm_ipv6_prefix_set(NULL, prefix_info, NULL, NULL, 0, 0);
    } else {
        uint32_t plt = GETP_UINT32(option, "Value.PreferredLifetime");
        uint32_t vlt = GETP_UINT32(option, "Value.ValidLifetime");
        calculate_lifetimes(plt, vlt, &preferred_lt, &valid_lt);
        amxc_string_setf(&full_prefix, "%s/%d", GETP_CHAR(option, "Value.Prefix"), GETP_UINT32(option, "Value.PrefixLen"));
        rv = dm_ipv6_prefix_set(&full_prefix, prefix_info, &preferred_lt, &valid_lt, plt, vlt);
    }

    when_failed_trace(rv, exit, ERROR, "Failed to set IAPD prefix in the datamodel");

exit:
    amxc_string_clean(&full_prefix);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void nm_router_advertisement_cb(UNUSED const char* sig_name,
                                       const amxc_var_t* result,
                                       void* userdata) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_prefix_info_t* prefix_info = NULL;
    amxc_ts_t preferred_lt;
    amxc_ts_t valid_lt;
    amxd_trans_t* trans = NULL;
    amxc_string_t full_prefix;

    amxc_string_init(&full_prefix, 0);

    when_null_trace(userdata, exit, ERROR, "No userdata provided");
    prefix_info = (ipv6_prefix_info_t*) userdata;

    SAH_TRACEZ_INFO(ME, "Received an update to prefix '%s'",
                    amxd_object_get_name(prefix_info->prefix_obj, AMXD_OBJECT_NAMED));

    trans = ip_manager_trans_create(prefix_info->prefix_obj);
    if((GET_ARG(result, "Prefix") == NULL) || (GET_BOOL(result, "Auto") == false)) {
        rv = dm_ipv6_prefix_set(NULL, prefix_info, NULL, NULL, 0, 0);
    } else {
        uint32_t plt = GET_UINT32(result, "PreferredLifetime");
        uint32_t vlt = GET_UINT32(result, "ValidLifetime");
        calculate_lifetimes(plt, vlt, &preferred_lt, &valid_lt);
        amxc_string_setf(&full_prefix, "%s/%d", GET_CHAR(result, "Prefix"), GET_UINT32(result, "PrefixLength"));
        rv = dm_ipv6_prefix_set(&full_prefix, prefix_info, &preferred_lt, &valid_lt, plt, vlt);
    }

    when_failed_trace(rv, exit, ERROR, "Failed to set Router Advertisement prefix in the datamodel");

    if(GET_ARG(result, "OnLink") != NULL) {
        amxd_trans_set_value(bool, trans, "OnLink", GET_BOOL(result, "OnLink"));
    }
    if(GET_ARG(result, "Auto") != NULL) {
        amxd_trans_set_value(bool, trans, "Autonomous", GET_BOOL(result, "Auto"));
    }

    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to additional prefix parameters in the datamodel");

exit:
    amxc_string_clean(&full_prefix);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void ipv6_prefix_stop_subscription(ipv6_prefix_info_t* prefix_info) {
    SAH_TRACEZ_IN(ME);
    when_null(prefix_info, exit);
    switch(prefix_info->prefix_origin) {
    case PREFIX_ORIGIN_PREFIXDELEGATION:
        nm_close_query(&prefix_info->nm_query);
        break;
    case PREFIX_ORIGIN_ROUTERADVERTISEMENT:
        nm_close_query(&prefix_info->nm_query);
        break;
    case PREFIX_ORIGIN_AUTOCONFIGURED:
        stop_listening_for_prefix_changes(new_ulaprefix_cb, prefix_info);
        break;
    case PREFIX_ORIGIN_CHILD:
        stop_listening_for_prefix_changes(new_prefix_cb, prefix_info);
        break;
    case PREFIX_ORIGIN_STATIC:
        if(prefix_info->prefix_type == PREFIX_STATIC_TYPE_CHILD) {
            stop_listening_for_prefix_changes(new_prefix_cb, prefix_info);
        }
        break;
    default:
        break;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int _ipv6_prefix_referenced_prefix_removed(UNUSED amxd_object_t* templ,
                                                  amxd_object_t* instance,
                                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(cstring_t, &trans, "ParentPrefix", "");
    rv = amxd_trans_apply(&trans, ip_manager_get_dm());

    when_failed_trace(rv, exit, ERROR, "Failed to clear the ParentPrefix for '%s'", amxd_object_get_name(instance, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int _ipv6_address_referenced_prefix_removed(UNUSED amxd_object_t* templ,
                                                   amxd_object_t* instance,
                                                   UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "");
    rv = amxd_trans_apply(&trans, ip_manager_get_dm());

    when_failed_trace(rv, exit, ERROR, "Failed to clear the Prefix for '%s'", amxd_object_get_name(instance, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool is_prefix_persistent(ipv6_prefix_info_t* prefix_info) {
    bool is_persistent = false;
    when_null_trace(prefix_info, exit, ERROR, "No prefix info provided")

    is_persistent = ((prefix_info->prefix_origin == PREFIX_ORIGIN_WELLKNOWN) ||
                     ((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) &&
                      prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC));

exit:
    return is_persistent;
}

void new_ulaprefix_cb(UNUSED const char* signame,
                      const amxc_var_t* const data,
                      void* const priv) {
    SAH_TRACEZ_IN(ME);
    new_prefix("ULAPrefix", data, priv);
    SAH_TRACEZ_OUT(ME);
}

void new_prefix_cb(UNUSED const char* signame,
                   const amxc_var_t* const data,
                   void* const priv) {
    SAH_TRACEZ_IN(ME);
    new_prefix("Prefix", data, priv);
    SAH_TRACEZ_OUT(ME);
}

int ipv6_prefix_apply(ipv6_prefix_info_t* prefix_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    bool enabled = false;
    when_null(prefix_info, exit);

    enabled = amxd_object_get_value(bool, prefix_info->prefix_obj, "Enable", NULL);
    when_false_status(enabled, exit, rv = 0);

    switch(prefix_info->prefix_origin) {
    case PREFIX_ORIGIN_PREFIXDELEGATION:
        rv = nm_open_dhcpoption_query(prefix_info->intf_obj, &prefix_info->nm_query, "req6", 25, nm_option25_cb, prefix_info);
        break;
    case PREFIX_ORIGIN_ROUTERADVERTISEMENT:
        rv = nm_open_dhcpoption_query(prefix_info->intf_obj, &prefix_info->nm_query, "ra", 3, nm_router_advertisement_cb, prefix_info);
        break;
    case PREFIX_ORIGIN_AUTOCONFIGURED:
        SAH_TRACEZ_INFO(ME, "New prefix '%s' origin: PREFIX_ORIGIN_AUTOCONFIGURED", amxd_object_get_name(prefix_info->prefix_obj, AMXD_OBJECT_NAMED));
        rv = ipv6_prefix_autoconfigured(prefix_info);
        break;
    case PREFIX_ORIGIN_CHILD:
        SAH_TRACEZ_INFO(ME, "New prefix '%s' origin: CHILD", amxd_object_get_name(prefix_info->prefix_obj, AMXD_OBJECT_NAMED));
        rv = ipv6_prefix_child(prefix_info);
        break;
    case PREFIX_ORIGIN_STATIC:
        if(prefix_info->prefix_type == PREFIX_STATIC_TYPE_CHILD) {
            SAH_TRACEZ_INFO(ME, "New prefix '%s' origin: STATIC StaticType: CHILD", amxd_object_get_name(prefix_info->prefix_obj, AMXD_OBJECT_NAMED));
            rv = ipv6_prefix_child(prefix_info);
        } else {
            SAH_TRACEZ_WARNING(ME, "Static prefixes with StaticType '%d' are not handled yet", prefix_info->prefix_type);
            if(!str_empty(GET_CHAR(amxd_object_get_param_value(prefix_info->prefix_obj, "Prefix"), NULL))) {
                rv = ip_manager_set_status(prefix_info->prefix_obj, STATUS_ENABLED);
            }
        }
        break;
    default:
        SAH_TRACEZ_INFO(ME, "Origin of prefix '%s' not handled", amxd_object_get_name(prefix_info->prefix_obj, AMXD_OBJECT_NAMED));
        break;
    }

    if(rv != 0) {
        ip_manager_set_status(prefix_info->prefix_obj, STATUS_ERROR);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_prefix_disable(ipv6_prefix_info_t* prefix_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    when_null_trace(prefix_info, exit, ERROR, "No prefix information provided to disable");

    ipv6_prefix_stop_subscription(prefix_info);
    dm_ipv6_prefix_set(NULL, prefix_info, NULL, NULL, 0, 0);
    ip_manager_set_status(prefix_info->prefix_obj, STATUS_DISABLED);

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _ipv6_prefix_added(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_tmpl_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_t* prefix_obj = amxd_object_get_instance(prefix_tmpl_obj, NULL, GET_UINT32(data, "index"));
    int rv = -1;

    when_null_trace(prefix_obj, exit, ERROR, "Prefix object could not be found");
    rv = init_prefix(prefix_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to initialise new IPv6 prefix");

    ipv6_prefix_apply((ipv6_prefix_info_t*) prefix_obj->priv);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_prefix_enable_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_prefix_info_t* prefix_info = NULL;
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);

    when_null(prefix_obj, exit);
    when_null(prefix_obj->priv, exit);
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;

    if(GETP_BOOL(data, "parameters.Enable.to")) {
        rv = ipv6_prefix_apply(prefix_info);
        when_failed_trace(rv, exit, ERROR, "Failed to enable IPv6Prefix '%s'", prefix_obj->name);
    } else {
        rv = ipv6_prefix_disable(prefix_info);
        when_failed_trace(rv, exit, ERROR, "Failed to disable IPv6Prefix '%s'", prefix_obj->name);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_prefix_changed(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_info_t* prefix_info = NULL;
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    const char* status = STATUS_ERROR;

    when_null(prefix_obj, exit);
    when_null(prefix_obj->priv, exit);
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;

    if(!amxd_object_get_value(bool, prefix_obj, "Enable", NULL)) {
        goto exit;
    }

    if((prefix_info->prefix_origin != PREFIX_ORIGIN_STATIC) && (prefix_info->prefix_type != PREFIX_STATIC_TYPE_STATIC)) {
        // Only handle prefixes with Origin/StaticType Static/Static here
        goto exit;
    }

    status = str_empty(GETP_CHAR(data, "parameters.Prefix.to")) ? STATUS_ERROR : STATUS_ENABLED;

    if(ip_manager_set_status(prefix_info->prefix_obj, status) != 0) {
        ip_manager_set_status(prefix_info->prefix_obj, STATUS_ERROR);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_child_prefix_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_info_t* prefix_info = NULL;
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    const char* parent_prefix = NULL;
    const amxc_ts_t* preferred_lt = NULL;
    const amxc_ts_t* valid_lt = NULL;
    amxc_string_t combined;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    amxc_var_t params;

    amxc_string_init(&combined, 0);
    amxc_var_init(&params);

    when_false(amxd_object_get_value(bool, prefix_obj, "Enable", NULL), exit); // If disabled, nothing needs to be done
    when_null(prefix_obj, exit);
    when_null(prefix_obj->priv, exit);
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;

    if((prefix_info->prefix_origin == PREFIX_ORIGIN_CHILD) ||
       ((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) &&
        (prefix_info->prefix_type == PREFIX_STATIC_TYPE_CHILD))) {
        const char* parent_prefix_path = GET_CHAR(amxd_object_get_param_value(prefix_obj, "ParentPrefix"), NULL);
        amxd_object_t* parent_prefix_obj = get_object_from_device_path(parent_prefix_path);
        parent_prefix = GET_CHAR(amxd_object_get_param_value(parent_prefix_obj, "Prefix"), NULL);
        amxd_object_get_params(parent_prefix_obj, &params, amxd_dm_access_protected);
        preferred_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
        valid_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));
        vlt = GET_UINT32(&params, "RelativeValidLifetime");
        plt = GET_UINT32(&params, "RelativePreferredLifetime");

    } else if(prefix_info->prefix_origin == PREFIX_ORIGIN_AUTOCONFIGURED) {
        parent_prefix = GET_CHAR(amxd_object_get_param_value(
                                     amxd_dm_get_object(ip_manager_get_dm(), "IP"),
                                     "ULAPrefix"),
                                 NULL);
    } else {
        goto exit;
    }

    when_str_empty_trace(parent_prefix, exit, INFO, "Parent prefix not yet filled in");

    ipv6_child_parent_combine(prefix_info,
                              GETP_CHAR(data, "parameters.ChildPrefixBits.to"),
                              parent_prefix, &combined, true);      // In case of failure, still set combined in DM to clear value

    dm_ipv6_prefix_set(&combined, prefix_info, preferred_lt, valid_lt, plt, vlt);
exit:
    amxc_string_clean(&combined);
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ula_prefix_changed(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_init(&ret);

    cfgctrlr_execute_function(NULL, "set-ulaprefix", GET_ARG(data, "parameters"), &ret);

    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t _ipv6_prefix_destroy(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   UNUSED const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_info_t* prefix_info = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "Object can not be NULL, invalid argument");
    when_null_trace(object->priv, exit, INFO, "Object has no private data, nothing to destroy");
    prefix_info = (ipv6_prefix_info_t*) object->priv;

    ipv6_prefix_stop_subscription(prefix_info);

    rv = ipv6_prefix_info_clean(&prefix_info);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _ipv6_prefix_removed(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_templ_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxc_string_t spath;

    amxc_string_init(&spath, 0);

    amxc_string_setf(&spath, ".^.^.*.IPv6Prefix.[ParentPrefix == 'Device.%s%d.']", GET_CHAR(data, "path"), GET_INT32(data, "index"));
    amxd_object_for_all(prefix_templ_obj, amxc_string_get(&spath, 0), _ipv6_prefix_referenced_prefix_removed, NULL);

    amxc_string_setf(&spath, ".^.^.*.IPv6Address.[Prefix == 'Device.%s%d.']", GET_CHAR(data, "path"), GET_INT32(data, "index"));
    amxd_object_for_all(prefix_templ_obj, amxc_string_get(&spath, 0), _ipv6_address_referenced_prefix_removed, NULL);

    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
}

void _ipv6_parent_prefix_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_prefix_info_t* prefix_info = NULL;
    const char* new_parent_prefix = GETP_CHAR(data, "parameters.ParentPrefix.to");
    bool enabled = amxd_object_get_value(bool, prefix_obj, "Enable", NULL);

    when_null_trace(prefix_obj, exit, ERROR, "No object found");
    when_null_trace(prefix_obj->priv, exit, WARNING, "Object has no private data, address can not be changed");
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;

    // ParentPrefix is only relevant for Child and StaticChild prefixes
    if((prefix_info->prefix_origin == PREFIX_ORIGIN_CHILD) ||
       ((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) &&
        (prefix_info->prefix_type == PREFIX_STATIC_TYPE_CHILD))) {
        // Stop listening on the old parent prefix
        ipv6_prefix_stop_subscription(prefix_info);

        when_false(enabled, exit);
        if((new_parent_prefix == NULL) || (*new_parent_prefix == '\0')) {
            // no new ParentPrefix, so clear the prefix
            dm_ipv6_prefix_set(NULL, prefix_info, NULL, NULL, 0, 0);
        } else {
            ipv6_prefix_apply(prefix_info);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_prefix_origin_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_prefix_info_t* prefix_info = NULL;
    ipv6_prefix_origin_t origin = prefix_origin_to_enum(GETP_CHAR(data, "parameters.Origin.to"));

    when_null_trace(prefix_obj, exit, ERROR, "No object found");
    when_null_trace(prefix_obj->priv, exit, ERROR, "Object has no private data, address can not be changed");
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;
    prefix_info->prefix_origin = origin;

    ipv6_prefix_toggle_persistency(prefix_obj, is_prefix_persistent(prefix_info));
    toggle_lifetime_read_only(prefix_obj, (origin != PREFIX_ORIGIN_STATIC));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_prefix_static_type_changed(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_prefix_info_t* prefix_info = NULL;
    ipv6_prefix_static_type_t type = prefix_static_type_to_enum(GETP_CHAR(data, "parameters.StaticType.to"));

    when_null_trace(prefix_obj, exit, ERROR, "No object found");
    when_null_trace(prefix_obj->priv, exit, ERROR, "Object has no private data, address can not be changed");
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;
    prefix_info->prefix_type = type;

    ipv6_prefix_toggle_persistency(prefix_obj, is_prefix_persistent(prefix_info));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_prefix_preferred_lt_changed(UNUSED const char* const sig_name,
                                       const amxc_var_t* const data,
                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_prefix_info_t* prefix_info = NULL;
    const amxc_ts_t* lifetime = NULL;
    char lt[36];

    when_null_trace(prefix_obj, exit, ERROR, "No object found");
    when_null_trace(prefix_obj->priv, exit, ERROR, "Object has no private data, address can not be changed");
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;

    amxp_timer_stop(prefix_info->preferred_timer);
    lifetime = amxc_var_constcast(amxc_ts_t, GETP_ARG(data, "parameters.PreferredLifetime.to"));
    when_null_trace(lifetime, exit, ERROR, "Failed to get the PreferredLifetime");

    if((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) &&
       (prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC)) {
        calculate_and_set_relative_lifetimes(lifetime, prefix_obj, "RelativePreferredLifetime");
    }

    amxc_ts_format_precision(lifetime, lt, sizeof(lt), 0);
    if(strncmp(lt, UNKNOWN_LIFETIME, 36) != 0) {
        preferred_timer_start(PREFIX_STATUS_PARAM, prefix_info->prefix_obj, prefix_info->preferred_timer);
    } else if(prefix_info->valid_timer->state != amxp_timer_running) {
        set_status(PREFIX_STATUS_PARAM, prefix_info->prefix_obj, PREFIX_STATUS_UNKNOWN);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_prefix_valid_lt_changed(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_prefix_info_t* prefix_info = NULL;
    const amxc_ts_t* lifetime = NULL;
    char lt[36];

    when_null_trace(prefix_obj, exit, ERROR, "No object found");
    when_null_trace(prefix_obj->priv, exit, ERROR, "Object has no private data, address can not be changed");
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;

    amxp_timer_stop(prefix_info->valid_timer);
    lifetime = amxc_var_constcast(amxc_ts_t, GETP_ARG(data, "parameters.ValidLifetime.to"));
    when_null_trace(lifetime, exit, ERROR, "Failed to get the ValidLifetime");

    if((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) &&
       (prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC)) {
        calculate_and_set_relative_lifetimes(lifetime, prefix_obj, "RelativeValidLifetime");
    }

    amxc_ts_format_precision(lifetime, lt, sizeof(lt), 0);
    if(strncmp(lt, UNKNOWN_LIFETIME, 36) != 0) {
        valid_timer_start(PREFIX_STATUS_PARAM, prefix_info->prefix_obj, prefix_info->valid_timer);
    } else if(prefix_info->preferred_timer->state != amxp_timer_running) {
        set_status(PREFIX_STATUS_PARAM, prefix_info->prefix_obj, PREFIX_STATUS_UNKNOWN);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int init_prefix(amxd_object_t* prefix_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_prefix_info_t* prefix_info = NULL;

    when_null_trace(prefix_obj, exit, WARNING, "No prefix object provided to initialise");
    when_not_null_trace(prefix_obj->priv, exit, WARNING, "Prefix object already initialised");
    rv = ipv6_prefix_info_new(&prefix_info, prefix_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to create prefix info structure");

    if((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) &&
       (prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC)) {
        init_relative_lifetimes(prefix_info->prefix_obj);
        lifetime_timers_start(PREFIX_STATUS_PARAM,
                              prefix_info->prefix_obj,
                              prefix_info->preferred_timer,
                              prefix_info->valid_timer);
    }

    rv = ipv6_prefix_toggle_persistency(prefix_info->prefix_obj, is_prefix_persistent(prefix_info));
    when_failed_trace(rv, exit, ERROR, "Failed to toggle persistency for '%s'", prefix_obj->name);
    rv = toggle_lifetime_read_only(prefix_info->prefix_obj, (prefix_info->prefix_origin != PREFIX_ORIGIN_STATIC));
    when_failed_trace(rv, exit, ERROR, "Failed to toggle read-only for '%s'", prefix_obj->name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
