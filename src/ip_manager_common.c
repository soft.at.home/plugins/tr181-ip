/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sys/sysinfo.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>

#include "ip_manager_ip.h"
#include "ipv4_utils.h"
#include "ipv6_utils.h"
#include "ip_manager_controller.h"
#include "ip_manager_interface.h"

#include "ip_manager_common.h"

#define ME "ip-mngr"

typedef struct _ip_manager {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxb_bus_ctx_t* ctx;
    bool nm_init;
} ip_manager_t;

ip_manager_t ip_manager;
ip_manager_ip_status_t* ipv4_query = NULL;
ip_manager_ip_status_t* ipv6_query = NULL;

amxd_dm_t* ip_manager_get_dm(void) {
    return ip_manager.dm;
}

amxo_parser_t* ip_manager_get_parser(void) {
    return ip_manager.parser;
}

const char* ip_manager_get_setting(const char* var) {
    amxc_var_t* setting = amxo_parser_get_config(ip_manager_get_parser(), var);
    return (setting != NULL) ? amxc_var_constcast(cstring_t, setting) : "";
}

int ip_manager_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    ip_manager.dm = dm;
    ip_manager.parser = parser;

    netmodel_initialize();

    // load the controllers
    rv = ip_manager_open_cfgctrlrs();
    when_failed_trace(rv, exit, ERROR, "Failed to load all supported modules");

    ipv4_query = ip_manager_populate_ip_status(IP_VERSION_4_FLAG, IP_VERSION_4);
    ipv6_query = ip_manager_populate_ip_status(IP_VERSION_6_FLAG, IP_VERSION_6);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void ip_manager_shutdown(void) {
    if(ipv4_query != NULL) {
        if(ipv4_query->lan_query != NULL) {
            netmodel_closeQuery(ipv4_query->lan_query);
            ipv4_query->lan_query = NULL;
        }
        if(ipv4_query->wan_query != NULL) {
            netmodel_closeQuery(ipv4_query->wan_query);
            ipv4_query->wan_query = NULL;
        }
        free(ipv4_query);
    }
    if(ipv6_query != NULL) {
        if(ipv6_query->lan_query != NULL) {
            netmodel_closeQuery(ipv6_query->lan_query);
            ipv6_query->lan_query = NULL;
        }
        if(ipv6_query->wan_query != NULL) {
            netmodel_closeQuery(ipv6_query->wan_query);
            ipv6_query->wan_query = NULL;
        }
        free(ipv6_query);
    }
    amxm_close_all();
    netmodel_cleanup();
}

amxd_trans_t* ip_manager_trans_create(const amxd_object_t* const object) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t* trans = NULL;

    when_failed(amxd_trans_new(&trans), exit);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(trans, object);

exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

amxd_status_t ip_manager_trans_apply(amxd_trans_t* trans) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    when_null(trans, exit);

    status = amxd_trans_apply(trans, ip_manager_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", status);
    }

    amxd_trans_delete(&trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

uint64_t get_system_uptime(void) {
    uint64_t uptime = 0;
    struct sysinfo info;
    when_failed_trace(sysinfo(&info), exit, ERROR, "Could not read total uptime of the system");
    uptime = info.uptime;
exit:
    return uptime;
}
