/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ipv4_utils.h"
#include "ip_manager_ip.h"
#include "ip_manager_interface.h"
#include "ip_manager_common.h"

#include "ip_manager_netdev.h"

#define ME "ip-mngr"

static amxd_status_t ip_manager_set_parameters(amxd_object_t* addr_obj,
                                               amxc_var_t* parameters,
                                               const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t* trans = NULL;

    trans = ip_manager_trans_create(addr_obj);
    amxd_trans_set_param(trans, "Scope", GET_ARG(parameters, "Scope"));
    amxd_trans_set_param(trans, "Flags", GET_ARG(parameters, "Flags"));
    amxd_trans_set_param(trans, "TypeFlags", GET_ARG(parameters, "TypeFlags"));
    amxd_trans_set_param(trans, "Peer", GET_ARG(parameters, "Peer"));
    // PrefixLen needs the type to be set to uint8_t for the transaction to be accepted
    amxd_trans_set_value(uint8_t, trans, "PrefixLen", GET_UINT32(parameters, "PrefixLen"));
    amxd_trans_set_value(cstring_t, trans, "Status", status);
    rv = ip_manager_trans_apply(trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Add the IP instance to the IP manager and fills in the addr_info structure
 *
 * @param ip_instance The IP variant to add to the ip-manager's data model
 * @param intf_name Name of the interface to which add the instance
 * @param ip_fam V6 or V4
 * @return amxd_object_t*
 */
static amxd_object_t* add_ipv6_to_ip_manager(amxc_var_t* ip_instance, const char* intf_name) {
    SAH_TRACEZ_IN(ME);

    amxd_trans_t trans;
    amxd_object_t* addr_object = NULL;
    amxd_object_t* addr_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_ts_t validlifetime;
    amxc_ts_t preferredlifetime;
    amxc_string_t the_flag;
    ipv6_addr_info_t* addr_info = NULL;
    int index = 0;
    const char* flag = NULL;
    const char* final_flag = NULL;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    amxc_llist_t matching_instances;

    amxd_trans_init(&trans);
    amxc_string_init(&the_flag, 0);
    amxc_llist_init(&matching_instances);

    when_null_trace(ip_instance, exit, ERROR, "IP instance to add is empty");
    when_str_empty_trace(GET_CHAR(ip_instance, "TypeFlags"), exit, ERROR, "Empty TypeFlags of the IP instance");
    when_str_empty_trace(intf_name, exit, WARNING, "Interface name empty, no ipv6 address added to ip-manager");

    /*
     *  Takes the TypeFlags paramater of the NetDev IP instance and
     *  creates the Alias with the following rule:
     *  TypeFlags = @lla -> Alias = LLA
     *  If the Alias already exist, the code puts an index at the end of it.
     *  Example LLA1 or LLA2 -> LLA[n].
     */
    amxc_string_set(&the_flag, GET_CHAR(ip_instance, "TypeFlags"));
    amxc_string_remove_at(&the_flag, 0, 1);
    amxc_string_to_upper(&the_flag);
    flag = amxc_string_get(&the_flag, 0);
    when_str_empty_trace(flag, exit, ERROR, "No flag set in '%s'", GET_CHAR(ip_instance, "TypeFlags"));
    when_true_trace((strcmp(flag, "GUA") == 0), exit, INFO, "Not going to add auto detected GUA address '%s'",
                    GET_CHAR(ip_instance, "Address"));

    //Checking if one or more instance(s) already has the same IP address
    amxd_dm_resolve_pathf(ip_manager_get_dm(),
                          &matching_instances,
                          "IP.Interface.[Name == '%s'].IPv6Address.[IPAddress == '%s'].",
                          intf_name,
                          GET_CHAR(ip_instance, "Address"));
    when_false_trace(amxc_llist_is_empty(&matching_instances), exit, WARNING, "Not going to duplicate this address %s on interface '%s'",
                     GET_CHAR(ip_instance, "Address"),
                     intf_name);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "IP.Interface.[Name == '%s'].IPv6Address.", intf_name);

    //Creating the Alias.
    addr_object = amxd_dm_findf(ip_manager_get_dm(),
                                "IP.Interface.[Name == '%s'].IPv6Address.[Alias == '%s'].",
                                intf_name,
                                flag);

    while(addr_object != NULL) {
        index++;
        addr_object = amxd_dm_findf(ip_manager_get_dm(),
                                    "IP.Interface.[Name == '%s'].IPv6Address.[Alias == '%s%d'].",
                                    intf_name,
                                    flag,
                                    index);
    }

    if(index != 0) {
        amxc_string_appendf(&the_flag, "%d", index);
    }
    final_flag = amxc_string_get(&the_flag, 0);

    amxd_trans_add_inst(&trans, 0, final_flag);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", GET_CHAR(ip_instance, "Address"));
    amxd_trans_set_value(cstring_t, &trans, "Origin", "WellKnown");
    amxd_trans_set_value(bool, &trans, "AutoDetected", true);

    plt = GET_UINT32(ip_instance, "PreferredLifetime");
    vlt = GET_UINT32(ip_instance, "ValidLifetime");
    if(plt == 0) {
        plt = UINT32_MAX;
    }
    if(vlt == 0) {
        vlt = UINT32_MAX;
    }
    calculate_lifetimes(plt, vlt, &preferredlifetime, &validlifetime);
    amxd_trans_set_value(uint32_t, &trans, "RelativePreferredLifetime", plt);
    if(amxc_ts_is_valid(&preferredlifetime)) {
        amxd_trans_set_value(amxc_ts_t, &trans, "PreferredLifetime", &preferredlifetime);
    }

    amxd_trans_set_value(uint32_t, &trans, "RelativeValidLifetime", vlt);
    if(amxc_ts_is_valid(&validlifetime)) {
        amxd_trans_set_value(amxc_ts_t, &trans, "ValidLifetime", &validlifetime);
    }

    status = amxd_trans_apply(&trans, ip_manager_get_dm());

    when_failed_trace(status, exit, ERROR,
                      "Failed to add instance '%s' on interface '%s', return '%d'",
                      final_flag,
                      intf_name,
                      status);

    addr_obj = amxd_dm_findf(ip_manager_get_dm(),
                             "IP.Interface.[Name == '%s'].IPv6Address.[Alias == '%s'].",
                             intf_name,
                             final_flag);

    when_null_trace(addr_obj, exit, ERROR, "Could not find address object %s and IP %s", intf_name, GET_CHAR(ip_instance, "Address"));

    ipv6_addr_info_new(&addr_info, addr_obj, true);
    when_null(addr_info, exit);

    SAH_TRACEZ_INFO(ME, "ipv6 address %s successfully set in the datamodel for '%s' in %s", GET_CHAR(ip_instance, "Address"), final_flag, intf_name);

exit:
    amxc_llist_clean(&matching_instances, amxc_string_list_it_free);
    amxc_string_clean(&the_flag);
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return addr_obj;
}

static void netdev_link_ip_addr_changed_cb(const char* const sig_name,
                                           const amxc_var_t* const data,
                                           void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t* trans = NULL;
    amxd_object_t* addr_obj = (amxd_object_t*) priv;

    when_null_trace(addr_obj, exit, ERROR, "Failed to update IP parameters, address object can not be NULL");
    SAH_TRACEZ_INFO(ME, "Received %s sig_name IP parameter changes for '%s' and index '%d'", sig_name, addr_obj->name, addr_obj->index);
    trans = ip_manager_trans_create(addr_obj);

    if(GETP_ARG(data, "parameters.Scope") != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Scope", GETP_CHAR(data, "parameters.Scope.to"));
    }
    if(GETP_ARG(data, "parameters.Flags") != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Flags", GETP_CHAR(data, "parameters.Flags.to"));
    }
    if(GETP_ARG(data, "parameters.TypeFlags") != NULL) {
        amxd_trans_set_value(cstring_t, trans, "TypeFlags", GETP_CHAR(data, "parameters.TypeFlags.to"));
    }
    if(GETP_ARG(data, "parameters.PrefixLen") != NULL) {
        amxd_trans_set_value(uint8_t, trans, "PrefixLen", GETP_UINT32(data, "parameters.PrefixLen.to"));
    }
    if(GETP_ARG(data, "parameters.Peer") != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Peer", GETP_CHAR(data, "parameters.Peer.to"));
    }

    if((GETP_ARG(data, "parameters.Scope.to") != NULL) || (GETP_ARG(data, "parameters.Flags.to") != NULL) ||
       (GETP_ARG(data, "parameters.TypeFlags.to") != NULL) || (GETP_ARG(data, "parameters.Peer.to") != NULL) ||
       (GETP_UINT32(data, "parameters.PrefixLen.to") != 0)) {
        rv = ip_manager_trans_apply(trans);
        if(rv != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to apply IP address parameters, return %d", rv);
        }
    } else {
        amxd_trans_delete(&trans);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

/*
 * Process an IPv4 address add or remove event on NetDev.Link.
 * Enable or disable the address accordingly.
 */
static void netdev_link_ipv4addr_instance_event_cb(UNUSED const char* const sig_name,
                                                   const amxc_var_t* const data,
                                                   void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = (amxd_object_t*) priv;
    amxd_object_t* ip_obj = NULL;
    const char* address = GETP_CHAR(data, "parameters.Address");
    const char* notification = GET_CHAR(data, "notification");
    uint32_t prefixlen = GETP_UINT32(data, "parameters.PrefixLen");
    char* netmask = ipv4_addr_get_netmask_from_prefixlen(prefixlen);
    bool enabled = false;

    when_str_empty(notification, exit);
    enabled = strcmp(notification, "dm:instance-added") == 0;

    SAH_TRACEZ_INFO(ME, "IPv4Address '%s/%d' %s NetDev.Link.[%s].",
                    address, prefixlen, enabled ? "added to" : "removed from",
                    amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));

    ip_obj = amxd_object_findf(intf_obj, "IPv4Address.[IPAddress == '%s' && SubnetMask == '%s']",
                               address, netmask);
    if(ip_obj != NULL) {
        if(enabled) {
            amxc_string_t path;
            amxc_string_init(&path, 0);
            amxc_string_setf(&path, "%s%d.", GET_CHAR(data, "path"), GET_UINT32(data, "index"));
            netdev_ipv4_addr_param_change_subscribe(ip_obj, amxc_string_get(&path, 0));
            amxc_string_clean(&path);
        } else {
            ip_manager_set_status(ip_obj, STATUS_DISABLED);
            SAH_TRACEZ_INFO(ME, "Found matching entry '%s', Status set to Disabled", ip_obj->name);
        }
    } else {
        SAH_TRACEZ_INFO(ME, "No matching entry found, [IPAddress:'%s', SubnetMask:'%s']", address, netmask);
    }

exit:
    free(netmask);
    SAH_TRACEZ_OUT(ME);
}

/**
 * @brief Removes the IP instance from the datamodel if auto generated
 *
 * @param addr_info The address info structure of the IP
 * @return amxd_status_t
 */
amxd_status_t ip_manager_rm_ip_instance(ipv6_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    const char* alias = NULL;

    amxd_trans_init(&trans);

    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    when_null_trace(addr_info->addr_obj, exit, ERROR, "No address object provided");
    alias = addr_info->addr_obj->name;

    SAH_TRACEZ_INFO(ME, "Delete IP from IP-manager Alias: %s", alias);
    status = amxd_status_ok;
    when_false_trace(addr_info->auto_detected, exit, INFO, "Not an autogenerated IP Alias: %s", alias);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, amxd_object_get_parent(addr_info->addr_obj));
    amxd_trans_del_inst(&trans, addr_info->addr_obj->index, NULL);

    status = amxd_trans_apply(&trans, ip_manager_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to apply transaction and deleting this IP from IP-manager with Alias: %s", alias);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static int handle_netdev_addr_add(amxd_object_t* intf_obj,
                                  const amxc_var_t* const data,
                                  const char* address,
                                  const char* intf_name) {
    int rv = -1;
    amxc_string_t path;
    amxd_object_t* addr_obj = amxd_object_findf(intf_obj, "IPv6Address.[IPAddress == '%s'].", address);
    amxc_string_init(&path, 0);

    if(addr_obj == NULL) {
        SAH_TRACEZ_INFO(ME, "No matching entry found, [IPAddress:'%s']. Adding it to '%s'", address, intf_name);
        addr_obj = add_ipv6_to_ip_manager(GET_ARG(data, "parameters"), intf_name);
        when_null_trace(addr_obj, exit, ERROR, "Failed to add '%s' to '%s'", address, intf_name);
    }

    amxc_string_setf(&path, "%s%d.", GET_CHAR(data, "path"), GET_UINT32(data, "index"));
    rv = netdev_ipv6_addr_param_change_subscribe(addr_obj, amxc_string_get(&path, 0));

exit:
    amxc_string_clean(&path);
    return rv;
}

static amxd_status_t handle_netdev_addr_remove(amxc_llist_t* matching_instances) {
    amxd_status_t rv = amxd_status_unknown_error;

    amxc_llist_iterate(it, matching_instances) {
        amxc_string_t* path = amxc_string_from_llist_it(it);
        ipv6_addr_info_t* addr_info = NULL;
        amxd_object_t* addr_obj = amxd_dm_findf(ip_manager_get_dm(), "%s", amxc_string_get(path, 0));

        when_null_trace(addr_obj, exit, WARNING, "No instance with path '%s' found", amxc_string_get(path, 0));
        ip_manager_set_status(addr_obj, STATUS_DISABLED);
        SAH_TRACEZ_INFO(ME, "Found matching entry '%s', Status set to Disabled", addr_obj->name);

        addr_info = (ipv6_addr_info_t*) addr_obj->priv;
        when_null_trace(addr_info, exit, WARNING, "Object does not contain address information");
        rv = ip_manager_rm_ip_instance(addr_info);
        when_failed_trace(rv, exit, ERROR, "Failed to delete IP instance, return '%d'", rv);
    }

exit:
    return rv;
}

/*
 * Process an IPv6 address add or remove event on NetDev.Link.
 * Enable or disable the address accordingly.
 */
static void netdev_link_ipv6addr_instance_event_cb(UNUSED const char* const sig_name,
                                                   const amxc_var_t* const data,
                                                   void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = (amxd_object_t*) priv;
    const char* address = GETP_CHAR(data, "parameters.Address");
    const char* notification = GET_CHAR(data, "notification");
    const char* intf_name = GET_CHAR(amxd_object_get_param_value(intf_obj, "Name"), NULL);
    bool enabled = false;
    amxc_llist_t matching_instances;
    amxc_llist_init(&matching_instances);

    when_str_empty(notification, exit);
    enabled = strcmp(notification, "dm:instance-added") == 0;

    SAH_TRACEZ_INFO(ME, "IPv6Address '%s' %s NetDev.Link.[%s].",
                    address, enabled ? "added to" : "removed from", intf_name);

    amxd_dm_resolve_pathf(ip_manager_get_dm(),
                          &matching_instances,
                          "IP.Interface.[Name == '%s'].IPv6Address.[IPAddress == '%s'].",
                          intf_name,
                          address);

    if(enabled) {
        if(amxc_llist_size(&matching_instances) > 1) {
            SAH_TRACEZ_ERROR(ME, "More than one instance found with IPAddress '%s'", address);
            handle_netdev_addr_remove(&matching_instances);
        }
        handle_netdev_addr_add(intf_obj, data, address, intf_name);
    } else {
        handle_netdev_addr_remove(&matching_instances);
    }

exit:
    amxc_llist_clean(&matching_instances, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
}

static int init_ipv4_addresses(const char* intf_name) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t netdev_link;
    amxc_string_t search_path;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");
    int rv = -1;

    amxc_var_init(&netdev_link);
    amxc_string_init(&search_path, 0);

    when_str_empty_trace(intf_name, exit, ERROR, "No interface name provided");
    rv = amxc_string_setf(&search_path, "NetDev.Link.[%s].IPv4Addr.*.", intf_name);
    when_failed_trace(rv, exit, ERROR, "Failed to set search path");

    rv = amxb_get(ctx, amxc_string_get(&search_path, 0), 0, &netdev_link, 3);

    amxc_var_for_each(ipv4_address_params, GET_ARG(&netdev_link, "0")) {
        const char* path = amxc_var_key(ipv4_address_params);
        uint32_t prefixlen = GET_UINT32(ipv4_address_params, "PrefixLen");
        const char* address = GET_CHAR(ipv4_address_params, "Address");
        char* netmask = ipv4_addr_get_netmask_from_prefixlen(prefixlen);
        amxd_object_t* addr_obj = amxd_dm_findf(ip_manager_get_dm(),
                                                "IP.Interface.[Name == '%s'].IPv4Address.[IPAddress == '%s' && SubnetMask == '%s'].",
                                                intf_name, address, netmask);
        free(netmask);
        if(addr_obj == NULL) {
            SAH_TRACEZ_WARNING(ME, "No IPv4Address found in IP-manager to match %s/%d", address, prefixlen);
            continue;
        }

        netdev_ipv4_addr_param_change_subscribe(addr_obj, path);
    }

exit:
    amxc_string_clean(&search_path);
    amxc_var_clean(&netdev_link);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int init_ipv6_addresses(const char* intf_name) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t netdev_link;
    amxc_string_t search_path;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");
    int rv = -1;

    amxc_var_init(&netdev_link);
    amxc_string_init(&search_path, 0);

    when_str_empty_trace(intf_name, exit, ERROR, "No interface name provided");

    if(amxc_string_setf(&search_path, "NetDev.Link.[%s].IPv6Addr.*.", intf_name) != 0) {
        goto exit;
    }

    rv = amxb_get(ctx, amxc_string_get(&search_path, 0), 0, &netdev_link, 3);

    amxc_var_for_each(ipv6_address_params, GET_ARG(&netdev_link, "0")) {
        const char* path = amxc_var_key(ipv6_address_params);
        const char* address = GET_CHAR(ipv6_address_params, "Address");
        amxd_object_t* addr_obj = amxd_dm_findf(ip_manager_get_dm(),
                                                "IP.Interface.[Name == '%s'].IPv6Address.[IPAddress == '%s'].",
                                                intf_name,
                                                address);
        if(addr_obj == NULL) {
            SAH_TRACEZ_WARNING(ME, "Adding this IPv6Address '%s' to IP manager.", address);
            addr_obj = add_ipv6_to_ip_manager(ipv6_address_params, intf_name);
            if(addr_obj != NULL) {
                SAH_TRACEZ_INFO(ME, "Automatically added this IP Address: %s with flag: %s to '%s'",
                                address, GET_CHAR(ipv6_address_params, "TypeFlags"), intf_name);
            } else {
                SAH_TRACEZ_ERROR(ME, "Failed to add this IPv6Address '%s' with this typeflag '%s' to '%s'.",
                                 address, GET_CHAR(ipv6_address_params, "TypeFlags"), intf_name);
                continue;
            }
        }

        if(addr_obj->priv != NULL) {
            netdev_ipv6_addr_param_change_subscribe(addr_obj, path);
        } else {
            SAH_TRACEZ_ERROR(ME, "No private data, unable to start subscription for IPv6 address '%s'", addr_obj->name);
        }
    }

exit:
    amxc_string_clean(&search_path);
    amxc_var_clean(&netdev_link);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/*
 * Create a NetDev subscription for instance add and remove events on NetDev.Link.[Alias]
 * These events are used to set the IPv4Address status in IP manager.
 * This callback will also set the initial state on all IPv4 addresses in this IP.Interface.
 */
static int dm_setup_ip_interface_ipv4addresses(amxd_object_t* ip_intf,
                                               const char* intf_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t expression;
    ip_intf_info_t* intf_priv = NULL;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");

    amxc_string_init(&expression, 0);
    when_null_trace(ip_intf, exit, ERROR, "ip_intf cannot be NULL");
    when_null_trace(ip_intf->priv, exit, ERROR, "Private data in ip_intf cannot be NULL");
    intf_priv = (ip_intf_info_t*) ip_intf->priv;

    amxc_string_setf(&expression, "notification in ['dm:instance-added','dm:instance-removed'] "
                     "&& eobject == 'NetDev.Link.[%s].IPv4Addr.'", intf_name);
    rv = amxb_subscription_new(&intf_priv->ndl_ipv4addr_subscr,
                               ctx,
                               "NetDev.Link.",
                               amxc_string_get(&expression, 0),
                               netdev_link_ipv4addr_instance_event_cb,
                               ip_intf);
    when_failed_trace(rv, exit, ERROR, "Failed to add IPv4 subscriptions");

    // Set initial IP address states
    init_ipv4_addresses(intf_name);

exit:
    amxc_string_clean(&expression);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/*
 * Create a NetDev subscription for instance add and remove events on NetDev.Link.[Alias]
 * These events are used to set the IPv6Address status in IP manager.
 * This callback will also set the initial state on all IPv6 addresses in this IP.Interface.
 */
static int dm_setup_ip_interface_ipv6addresses(amxd_object_t* ip_intf,
                                               const char* intf_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t expression;
    ip_intf_info_t* intf_priv = NULL;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");

    amxc_string_init(&expression, 0);
    when_null_trace(ip_intf, exit, ERROR, "ip_intf cannot be NULL");
    when_null_trace(ip_intf->priv, exit, ERROR, "Private data in ip_intf cannot be NULL");
    intf_priv = (ip_intf_info_t*) ip_intf->priv;

    amxc_string_setf(&expression, "notification in ['dm:instance-added','dm:instance-removed'] "
                     "&& eobject == 'NetDev.Link.[%s].IPv6Addr.'", intf_name);
    rv = amxb_subscription_new(&intf_priv->ndl_ipv6addr_subscr,
                               ctx,
                               "NetDev.Link.",
                               amxc_string_get(&expression, 0),
                               netdev_link_ipv6addr_instance_event_cb,
                               ip_intf);
    when_failed_trace(rv, exit, ERROR, "Failed to add IPv6 subscriptions");

    // Set initial IP address states
    init_ipv6_addresses(intf_name);

exit:
    amxc_string_clean(&expression);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int netdev_ipv4_addr_param_change_subscribe(amxd_object_t* addr_obj,
                                            const char* const path) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t netdev_link;
    ipv4_addr_info_t* addr_info = NULL;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");

    amxc_var_init(&netdev_link);

    when_str_empty_trace(path, exit, INFO, "No path provided to subscribe to");
    when_null_trace(addr_obj, exit, ERROR, "No IPv4 address object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in this IPv4 address object found");
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;

    SAH_TRACEZ_INFO(ME, "Create new IPv4 address parameter subscription");

    ipv4_addr_info_subscription_clean(addr_info);
    rv = amxb_subscription_new(&addr_info->ipv4_param_subscr,
                               ctx,
                               path,
                               "notification == 'dm:object-changed'",
                               netdev_link_ip_addr_changed_cb,
                               addr_obj);

    amxb_get(ctx, path, 0, &netdev_link, 3);
    ip_manager_set_parameters(addr_obj, GETP_ARG(&netdev_link, "0.0"), STATUS_ENABLED);

exit:
    amxc_var_clean(&netdev_link);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int netdev_ipv6_addr_param_change_subscribe(amxd_object_t* addr_obj,
                                            const char* const path) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t netdev_link;
    ipv6_addr_info_t* addr_info = NULL;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");

    amxc_var_init(&netdev_link);

    when_str_empty_trace(path, exit, INFO, "No path provided to subscribe to");
    when_null_trace(addr_obj, exit, ERROR, "No IPv6 address object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in this IPv6 address object found");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    SAH_TRACEZ_INFO(ME, "Create new IPv6 address parameter subscription");

    ipv6_addr_info_subscription_clean(addr_info);
    rv = amxb_subscription_new(&addr_info->ipv6_param_subscr,
                               ctx,
                               path,
                               "notification == 'dm:object-changed'",
                               netdev_link_ip_addr_changed_cb,
                               addr_obj);

    amxb_get(ctx, path, 0, &netdev_link, 3);
    ip_manager_set_parameters(addr_obj, GETP_ARG(&netdev_link, "0.0"), STATUS_ENABLED);

exit:
    amxc_var_clean(&netdev_link);
    SAH_TRACEZ_OUT(ME);
    return rv;
}


/**
 * @brief Creates the NetDev subscription for instance add and remove events on NetDev.Link.[Alias]
 * These events are used to set the IPv4/6 address status in IP manager.
 * It will also set the initial state on all IPv4/6 addresses in this IP.Interface.
 * @param intf_obj pointer to the IP interface object
 * @param name the NetDev Name (Alias) of the interface
 * @return 0 if the function was able to create the subscriptions without errors, otherwise it returns -1
 */
int ip_address_open_subscriptions(amxd_object_t* intf_obj, const char* name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    // Subscribe to IPv4 events and get some initial info
    rv = dm_setup_ip_interface_ipv4addresses(intf_obj, name);
    when_failed_trace(rv, exit, ERROR, "Unable to add IPv4 address subscriptions to NetDev.Link '%s'", name);
    // Subscribe to IPv6 events and get some initial info
    rv = dm_setup_ip_interface_ipv6addresses(intf_obj, name);
    when_failed_trace(rv, exit, ERROR, "Unable to add IPv6 address subscriptions to NetDev.Link '%s'", name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
