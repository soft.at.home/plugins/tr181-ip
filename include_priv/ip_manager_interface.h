/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__TR_IP_INTERFACE_H__)
#define __TR_IP_INTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <netmodel/client.h>

#define INTF_TYPE_STR_ETHERNET "eth"
#define INTF_TYPE_STR_VLAN     "vlan"
#define INTF_TYPE_STR_PPP      "ppp"

#define str_empty(x) ((x == NULL) || (*x == '\0'))
// Manages private data for an IP.Interface.{} object
typedef struct _ip_intf_info {
    time_t last_changed_timestamp;
    amxd_object_t* obj;                                     // Pointer to this IP.Interface.{} DM object
    amxb_subscription_t* ndl_ipv4addr_subscr;               // To manage the NetDev.Link.i.IPv4Address.j instance-added/remove subscriptions
    amxb_subscription_t* ndl_ipv6addr_subscr;               // To manage the NetDev.Link.i.IPv6Address.j instance-added/remove subscriptions
    netmodel_query_t* name_query;                           // netmodel query for the interface netdev name
    netmodel_query_t* status_query;                         // netmodel query for the external interface status
    char* intf_name;                                        // netdev name of the interface
    char* nm_intf_path;                                     // Path to the NetModel interface which provides the netdev name
    netmodel_query_t* mtu_name_query;
    char* mtu_intf_name;
    uint32_t old_mtu;
} ip_intf_info_t;

typedef struct {
    netmodel_query_t* lan_query;                           // netmodel query for the lan interface
    netmodel_query_t* wan_query;                           // netmodel query for the wan interface
    bool ip_lan;
    bool ip_wan;
    uint8_t ip_version;
} ip_manager_ip_status_t;

typedef enum _intf_type {
    INTF_TYPE_ETHERNET,
    INTF_TYPE_VLAN,
    INTF_TYPE_PPP,
    INTF_TYPE_CELLULAR,
} intf_type_t;

int ip_intf_info_new(ip_intf_info_t** intf_info, amxd_object_t* intf_obj);
int ip_intf_info_clean(ip_intf_info_t** intf_info);
int ip_interface_subscriptions_clean(ip_intf_info_t* ip_intf);
amxd_status_t _Reset(amxd_object_t* intf,
                     amxd_function_t* func,
                     amxc_var_t* args,
                     amxc_var_t* ret);
amxd_status_t _interface_destroy(amxd_object_t* intf,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv);
amxd_status_t _lastchange_read(amxd_object_t* intf,
                               amxd_param_t* param, amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval, void* priv);
amxd_status_t _reset_read(amxd_object_t* intf,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv);


int ip_interface_set_name(amxd_object_t* intf_obj, const char* name);
void handle_new_netdevname(ip_intf_info_t* intf, const char* new_name);
int nm_open_netdevname_query(amxd_object_t* intf_obj);

void ipv4_toggle_addresses(amxd_object_t* intf_obj, bool enable);
bool ipv6_toggle_addresses(amxd_object_t* intf_obj, bool enable);
void ipv6_toggle_prefixes(amxd_object_t* intf_obj, bool enable);
bool netdev_name_known(amxd_object_t* intf_obj);

char* get_nm_intf_by_netdev_name(const char* netdev_name);
int toggle_ip_flags_nm(ip_intf_info_t* intf, const char* flags, bool set);

void _ip_interface_added(const char* const sig_name,
                         const amxc_var_t* const data,
                         void* const priv);

void _ip_interface_lower_layers_changed(const char* const sig_name,
                                        const amxc_var_t* const data,
                                        void* const priv);

void _set_max_mtu_size(const char* const sig_name,
                       const amxc_var_t* const data,
                       void* const priv);

void _neigh_reachable_time_changed(const char* const sig_name,
                                   const amxc_var_t* const data,
                                   void* const priv);

void _enable_toggle_interface(const char* const sig_name,
                              const amxc_var_t* const data,
                              void* const priv);

void _ipv4_toggle_interface(const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv);

void _ipv6_toggle_interface(const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv);

void _ipv4_toggle_all(const char* const sig_name,
                      const amxc_var_t* const data,
                      void* const priv);

void _ipv6_toggle_all(const char* const sig_name,
                      const amxc_var_t* const data,
                      void* const priv);

void _reset_interface(const char* const sig_name,
                      const amxc_var_t* const data,
                      void* const priv);

intf_type_t intf_get_lowerlayer_type(const char* llayer);
const char* intf_get_lowerlayer_type_str(const char* llayer);
ip_manager_ip_status_t* ip_manager_populate_ip_status(const char* flag, uint8_t IP_version);
int intf_name_close_query(ip_intf_info_t* priv, bool free_name);

amxd_status_t _ipv6_interface_going_down(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv);

amxd_status_t _ipv6_going_down(amxd_object_t* object,
                               amxd_param_t* param,
                               amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               void* priv);

#ifdef __cplusplus
}
#endif

#endif // __TR_IP_INTERFACE_H__
