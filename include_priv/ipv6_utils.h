/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__TR_IPV6_UTILS_H__)
#define __TR_IPV6_UTILS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxp/amxp_timer.h>
#include <netmodel/common_api.h>

#define INFINITE_LIFETIME "9999-12-31T23:59:59Z"
#define UNKNOWN_LIFETIME "0001-01-01T00:00:00Z"

typedef enum _intf_id {
    UNKNOWN_INTF_ID,
    EUI64_INTF_ID,
    FIXED_INTF_ID,
    NONE_INTF_ID,
} intf_id_t;

typedef enum _ipv6_addr_origin {
    ADDR_ORIGIN_UNKNOWN,
    ADDR_ORIGIN_AUTOCONFIGURED,
    ADDR_ORIGIN_DHCPV6,
    ADDR_ORIGIN_IKEV2,
    ADDR_ORIGIN_MAP,
    ADDR_ORIGIN_WELLKNOWN,
    ADDR_ORIGIN_STATIC,
} ipv6_addr_origin_t;

typedef enum _ipv6_prefix_origin {
    PREFIX_ORIGIN_UNKNOWN,
    PREFIX_ORIGIN_AUTOCONFIGURED,
    PREFIX_ORIGIN_PREFIXDELEGATION,
    PREFIX_ORIGIN_ROUTERADVERTISEMENT,
    PREFIX_ORIGIN_WELLKNOWN,
    PREFIX_ORIGIN_STATIC,
    PREFIX_ORIGIN_CHILD,
} ipv6_prefix_origin_t;

typedef enum _ipv6_prefix_static_type {
    PREFIX_STATIC_TYPE_UNKNOWN,
    PREFIX_STATIC_TYPE_STATIC,
    PREFIX_STATIC_TYPE_INAPPLICABLE,
    PREFIX_STATIC_TYPE_PREFIXDELEGATION,
    PREFIX_STATIC_TYPE_CHILD,
} ipv6_prefix_static_type_t;

typedef struct _ipv6_prefix_info {
    amxd_object_t* prefix_obj;                          // pointer to object it self
    amxd_object_t* intf_obj;                            // pointer to interface this prefix belongs to
    ipv6_prefix_origin_t prefix_origin;                 // The prefix origin type
    ipv6_prefix_static_type_t prefix_type;              // The prefix StaticType in enum form
    netmodel_query_t* nm_query;                         // pointer to a NetModel query
    amxp_timer_t* preferred_timer;
    amxp_timer_t* valid_timer;
} ipv6_prefix_info_t;

typedef struct _ipv6_addr_info {
    amxd_object_t* addr_obj;                            // pointer to object it self
    amxd_object_t* intf_obj;                            // pointer to interface object this ipv6 is part of
    intf_id_t intf_id_type;                             // The interface id type for this IPAddress
    netmodel_query_t* nm_query;                         // pointer to a NetModel query
    ipv6_addr_origin_t origin;                          // Origin of the address in enum form
    amxb_subscription_t* ipv6_param_subscr;             // Subscriptions for changes to the IP address parameters in NetDev
    amxp_timer_t* preferred_timer;
    amxp_timer_t* valid_timer;
    bool auto_detected;
} ipv6_addr_info_t;

#define LIFETIME_STATUS_PREFERRED "Preferred"
#define LIFETIME_STATUS_DEPRECATED "Deprecated"
#define LIFETIME_STATUS_INVALID "Invalid"
#define STATUS_INACCESSIBLE "Inaccessible"
#define STATUS_UNKNOWN "Unknown"

#define PREFIX_STATUS_PREFERRED LIFETIME_STATUS_PREFERRED
#define PREFIX_STATUS_DEPRECATED LIFETIME_STATUS_DEPRECATED
#define PREFIX_STATUS_INVALID LIFETIME_STATUS_INVALID
#define PREFIX_STATUS_INACCESSIBLE STATUS_INACCESSIBLE
#define PREFIX_STATUS_UNKNOWN STATUS_UNKNOWN

#define ADDR_STATUS_PREFERRED LIFETIME_STATUS_PREFERRED
#define ADDR_STATUS_DEPRECATED LIFETIME_STATUS_DEPRECATED
#define ADDR_STATUS_INVALID LIFETIME_STATUS_INVALID
#define ADDR_STATUS_INACCESSIBLE STATUS_INACCESSIBLE
#define ADDR_STATUS_UNKNOWN STATUS_UNKNOWN
#define ADDR_STATUS_TENTATIVE "Tentative"
#define ADDR_STATUS_DUPLICATE "Duplicate"
#define ADDR_STATUS_OPTIMISTIC "Optimistic"

#define ADDR_STATUS_PARAM "IPAddressStatus"
#define PREFIX_STATUS_PARAM "PrefixStatus"

amxd_status_t set_status(const char* param_name, amxd_object_t* obj, const char* status);
amxd_status_t ipv6_prefix_info_new(ipv6_prefix_info_t** prefix_info, amxd_object_t* prefix_obj);
amxd_status_t ipv6_addr_info_new(ipv6_addr_info_t** addr_info,
                                 amxd_object_t* addr_obj,
                                 bool auto_detected);
int ipv6_child_parent_combine(ipv6_prefix_info_t* prefix_info, const char* child, const char* parent, amxc_string_t* combined, bool cidr);
ipv6_prefix_origin_t prefix_origin_to_enum(const char* s_origin);
ipv6_prefix_static_type_t prefix_static_type_to_enum(const char* s_type);
intf_id_t convert_to_intf_id(const char* s_intf_id_type);
amxd_object_t* get_object_from_device_path(const char* device_path);
amxd_object_t* get_prefix_object_from_addr(amxd_object_t* addr_obj);
int dm_ipv6_set(const char* param, const char* new_value, const char* status_param_name,
                amxd_object_t* obj, uint32_t plt, uint32_t vlt,
                const amxc_ts_t* preferred_lt, const amxc_ts_t* valid_lt,
                amxp_timer_t* preferred_timer, amxp_timer_t* valid_timer);
int ipv6_add_eui(amxd_object_t* intf_obj, amxc_string_t* ipv6_address);
int ipv6_add_fixed(ipv6_addr_info_t* addr_info, amxc_string_t* ipv6_address);
void start_listening_for_changes(const char* prefix_path, const char* parameter, amxp_slot_fn_t fn, void* const priv);
void stop_listening_for_prefix_changes(amxp_slot_fn_t fn, void* const priv);
bool is_dynamic_ip(const char* s_origin);
int ipv6address_build_ctrlr_args_from_object(amxd_object_t* obj, amxc_var_t* ctrlr_args);
int calculate_lifetimes(uint32_t plt, uint32_t vlt, amxc_ts_t* preferred_lt, amxc_ts_t* valid_lt);
int ipv6_addr_disable(ipv6_addr_info_t* addr_info);
ipv6_addr_origin_t addr_origin_to_enum(const char* s_origin);
int preferred_timer_start(const char* status_param_name, amxd_object_t* obj, amxp_timer_t* preferred_timer);
int valid_timer_start(const char* status_param_name, amxd_object_t* obj, amxp_timer_t* valid_timer);
int toggle_lifetime_read_only(amxd_object_t* obj, bool is_read_only);
int calculate_and_set_relative_lifetimes(const amxc_ts_t* lifetime, amxd_object_t* obj, const char* lifetime_name);
void init_relative_lifetimes(amxd_object_t* obj);
void lifetime_timers_start(const char* status_param_name, amxd_object_t* obj, amxp_timer_t* preferred_timer, amxp_timer_t* valid_timer);

amxd_status_t ipv6_prefix_info_clean(ipv6_prefix_info_t** prefix_info);
amxd_status_t ipv6_addr_info_clean(ipv6_addr_info_t** addr_info);
int ipv6_addr_info_subscription_clean(ipv6_addr_info_t* addr_info);
int dm_ipv6_address_set(const char* ipv6_address, ipv6_addr_info_t* addr_info, const amxc_ts_t* preferred_lt, const amxc_ts_t* valid_lt, const uint32_t plt, const uint32_t vlt);
int dm_ipv6_prefix_set(const amxc_string_t* prefix, ipv6_prefix_info_t* prefix_info, const amxc_ts_t* preferred_lt, const amxc_ts_t* valid_lt, uint32_t plt, uint32_t vlt);
int ipv6_intf_id_set(ipv6_addr_info_t* addr_info, amxc_string_t* ipv6_address);
int ipv6_generate_and_set_in_dm(ipv6_addr_info_t* addr_info, const char* prefix_path);
int ipv6_prefix_autoconfigured(ipv6_prefix_info_t* prefix_info);
int ipv6_prefix_child(ipv6_prefix_info_t* prefix_info);

#ifdef __cplusplus
}
#endif

#endif // __TR_IPV6_UTILS_H__
