# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.47.21 - 2024-12-06(15:17:58 +0000)

### Other

- [odl-generator] add support for failover IP interface

## Release v1.47.20 - 2024-12-03(11:02:07 +0000)

### Other

- - [routeradvertisement] A and L flags must be set by default

## Release v1.47.19 - 2024-11-25(16:40:07 +0000)

### Other

- ULA prefix has incorrect StaticType

## Release v1.47.18 - 2024-11-22(13:22:50 +0000)

### Other

- [IPv6 static] Static LAN prefix not supported

## Release v1.47.17 - 2024-11-20(15:20:04 +0000)

### Other

- - [ip-manager] declare custom events in odl definitions

## Release v1.47.16 - 2024-11-08(14:02:27 +0000)

### Other

- Reduce regex usage in signal handling

## Release v1.47.15 - 2024-10-10(10:16:43 +0000)

### Other

- [IPv6 static] Static LAN prefix not supported

## Release v1.47.14 - 2024-09-23(12:38:12 +0000)

### Other

- CLONE - Network Connection page parameters must be registered with PCM

## Release v1.47.13 - 2024-09-13(09:19:20 +0000)

### Other

- - [Terminating dot][tr181] fix IP

## Release v1.47.12 - 2024-09-12(15:17:25 +0000)

### Other

- - switch from a ethernet ll to a ppp ll on enabled intf will not remove existing addresses
- - IPv6 addresses not removed after changing lowerlayers from ethernet to PPP on an enabled interface

## Release v1.47.11 - 2024-09-12(09:52:06 +0000)

### Other

- - [Terminating dot][tr181] fix IP

## Release v1.47.10 - 2024-09-10(07:17:31 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.47.9 - 2024-09-05(07:40:58 +0000)

### Other

- [NetModel] Connect via direct socket if possible

## Release v1.47.8 - 2024-08-30(09:45:49 +0000)

## Release v1.47.7 - 2024-08-14(11:28:17 +0000)

### Other

- - GUA_IAPD instances have a wrong StaticType

## Release v1.47.6 - 2024-07-23(07:42:56 +0000)

### Fixes

- Better shutdown script

## Release v1.47.5 - 2024-07-18(14:33:34 +0000)

### Other

- - Rework prefix generation

## Release v1.47.4 - 2024-07-08(14:52:48 +0000)

### Other

- - Differentiate a static and a dynamic on mode on the wan interface for IPv6 Prefix

## Release v1.47.3 - 2024-07-02(13:14:34 +0000)

## Release v1.47.2 - 2024-06-24(09:55:45 +0000)

### Other

- - Do not generate prefix if ChildPrefixBits will have no effect

## Release v1.47.1 - 2024-06-17(15:24:59 +0000)

### Other

- - IPv6 Addresses are not always set back after interface goes down

## Release v1.47.0 - 2024-06-10(07:19:04 +0000)

### New

- - [Cellular] Add support of cellular interfaces

## Release v1.46.8 - 2024-05-30(11:51:28 +0000)

### Other

- - [tr181-ppp] Sometimes the lla address on the pppoe-wan interface is not there

## Release v1.46.7 - 2024-05-17(20:44:06 +0000)

### Fixes

- correct default ipv6 configuration for AP

## Release v1.46.6 - 2024-05-02(08:10:19 +0000)

### Fixes

- AP config DHCP addressing on bridge interface
- give bridges on access point 192.168.x.254 as IP

## Release v1.46.5 - 2024-04-25(11:03:49 +0000)

### Fixes

- DSLite-entry and DSLite-exit will both get Name=dslite0

## Release v1.46.4 - 2024-04-23(10:16:35 +0000)

### Other

- dynamically add ip configuration depending on the bridges in...

## Release v1.46.3 - 2024-04-11(16:25:19 +0000)

### Fixes

- Error for disabled instances when initializing

## Release v1.46.2 - 2024-04-10(09:59:35 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.46.1 - 2024-03-29(11:37:53 +0000)

### Fixes

- Duplicate instance of autodetected addresses

## Release v1.46.0 - 2024-03-25(08:32:01 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.45.2 - 2024-03-25(07:19:44 +0000)

### Fixes

- Error in Enum value

## Release v1.45.1 - 2024-03-19(12:37:34 +0000)

### Fixes

- DHCP_IAPD prefix is disabled instead of DHCP_IAPD Address

## Release v1.45.0 - 2024-03-15(17:51:51 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.44.4 - 2024-03-14(16:56:57 +0000)

## Release v1.44.3 - 2024-03-14(15:46:33 +0000)

### Fixes

- Rework unit tests

## Release v1.44.2 - 2024-03-05(11:31:32 +0000)

### Fixes

- CLONE - [IPv6] After deactivating IPv6, the DUT does not send a DHCPv6 Release message.

## Release v1.44.1 - 2024-03-04(19:10:19 +0000)

### Other

- [TR181-IP] Router WAN ARP renewal frequency configuration

## Release v1.44.0 - 2024-03-01(19:17:00 +0000)

### New

- Make IPv6 upgrade persistent

## Release v1.43.1 - 2024-03-01(11:12:58 +0000)

### Fixes

- Rework the startup

## Release v1.43.0 - 2024-02-21(14:53:14 +0000)

### New

- No WAN GUA assigned from delegated prefix in numbered mode

## Release v1.42.1 - 2024-02-10(08:36:43 +0000)

### Fixes

- lighttpd sometimes does not start

## Release v1.42.0 - 2024-02-08(16:21:42 +0000)

### New

- [TR181-IP] Router WAN ARP renewal frequency configuration

## Release v1.41.10 - 2024-01-22(13:04:37 +0000)

### Other

- Implement Device.IP.Interface.{i}.Reset

## Release v1.41.9 - 2024-01-10(12:06:33 +0000)

### Other

- Fix documentation generation
- Changing LowerLayers parameter will not remove IPs from the...

## Release v1.41.8 - 2023-12-14(16:45:04 +0000)

### Other

- - [PRPL] Components with files for GW and AP: improve command to install them

## Release v1.41.7 - 2023-12-13(16:28:22 +0000)

### Other

- Set `lla` controllers by default

## Release v1.41.6 - 2023-12-12(11:34:21 +0000)

### Fixes

- [Repeater Config][IPv6] Implement a proper IPv6 config of the repeater

## Release v1.41.5 - 2023-11-30(12:36:08 +0000)

### Fixes

- [Guest Bridge] Add guest support on AP, using ethernet backhaul.

## Release v1.41.4 - 2023-11-23(16:30:12 +0000)

### Other

- -IPv6Enable is neither reboot nor upgrade persistent.

## Release v1.41.3 - 2023-10-25(10:06:47 +0000)

### Other

- [ACL][IP-Manager] Allow write permission for CWMP on all IP datamodel

## Release v1.41.2 - 2023-10-17(11:41:06 +0000)

### Fixes

-  [IPv6 Enable][UserSetting] It must be possible to Enable Ipv6 on the HGW

## Release v1.41.1 - 2023-10-13(13:48:57 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.41.0 - 2023-10-09(07:07:04 +0000)

### New

- [amxrt][no-root-user][capability drop] tr181-ip plugin must be adapted to run as non-root and lmited capabilities

## Release v1.40.0 - 2023-09-26(11:39:07 +0000)

### New

- [AP Config][IPv4 connectivity] Repeater should switch from static to dhcp ip address

## Release v1.39.4 - 2023-09-22(13:10:18 +0000)

### Fixes

- [cdrouter] Verify basic MSS Clamping for TCP sessions test fails

## Release v1.39.3 - 2023-09-19(15:09:53 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v1.39.2 - 2023-09-01(13:53:48 +0000)

### Fixes

- [TR181-IP] Make MTU configurable

## Release v1.39.1 - 2023-08-31(09:14:20 +0000)

### Fixes

- After upgrade DHCP is a not answering

## Release v1.39.0 - 2023-07-26(11:48:36 +0000)

### New

- [tr181-pcm] Set the usersetting parameters for each plugin

## Release v1.38.0 - 2023-07-17(09:30:26 +0000)

### New

- [TR181 IP manager] IPv4/6Status do not reflect their respective IP status

## Release v1.37.0 - 2023-06-07(08:19:57 +0000)

### New

- add ACLs permissions for cwmp user

## Release v1.36.3 - 2023-05-23(15:24:42 +0000)

## Release v1.36.2 - 2023-05-08(08:46:50 +0000)

### Fixes

- Use correct status parameter name

## Release v1.36.1 - 2023-05-08(07:15:59 +0000)

### Fixes

- [TR-181][IP-Manager] For PPP the IPv6 address should not be set automatically

## Release v1.36.0 - 2023-04-24(10:32:25 +0000)

### New

- Make lifetimes functional

## Release v1.35.0 - 2023-04-12(14:04:27 +0000)

### New

- [TR-181][IP-Manager] It must be possible to configure a gateway in Numbered or Unnumbered mode

## Release v1.34.0 - 2023-04-12(12:09:23 +0000)

### New

- Support IPv6 unnumbered mode

## Release v1.33.11 - 2023-04-05(16:39:44 +0000)

### Fixes

- tr181-ip *.LastChanges are wrong in datamodel

## Release v1.33.10 - 2023-03-31(09:56:50 +0000)

### Fixes

- Allow netdev names to have a dot in them when calling rpc functions

## Release v1.33.9 - 2023-03-30(06:45:46 +0000)

### Changes

- Cleanup traces

## Release v1.33.8 - 2023-03-22(08:35:27 +0000)

### Fixes

- Add missing default AP odl files

## Release v1.33.7 - 2023-03-17(18:39:18 +0000)

### Other

- [baf] Correct typo in config option

## Release v1.33.6 - 2023-03-16(13:15:10 +0000)

### Other

- Add AP config files
- [Config] enable configurable coredump generation

## Release v1.33.5 - 2023-03-07(13:19:26 +0000)

### Other

- Interface name not cleared when interface is disabled

## Release v1.33.4 - 2023-03-03(10:41:44 +0000)

### Fixes

- GUA_IAPD prefix on bridge interfaces should not be origin Child

## Release v1.33.3 - 2023-03-02(13:51:26 +0000)

### Fixes

- PPPOE wanmode stopped working

## Release v1.33.2 - 2023-02-28(18:23:51 +0000)

### Other

- Changing Subnetmask from static IP doesn't remove the old IP

## Release v1.33.1 - 2023-02-28(18:10:33 +0000)

### Fixes

- Box sends 2 ICMPv6 RA when a RS is received on LAN

### Other

- Add missing runtime dependency on rpcd

## Release v1.33.0 - 2023-02-23(11:52:50 +0000)

### New

- Box sends 2 ICMPv6 RA when a RS is received on LAN

## Release v1.32.10 - 2023-02-10(15:28:28 +0000)

### Fixes

- Improve stability

## Release v1.32.9 - 2023-02-02(08:58:42 +0000)

### Fixes

- No IPv4 queries created if LowerLayers is empty during enable

## Release v1.32.8 - 2023-01-27(10:30:46 +0000)

### Fixes

- GUA_RA IPv6Address is not filled in for ppp6

## Release v1.32.7 - 2023-01-27(09:52:04 +0000)

### Other

- Configure static IPv4 and IPv6 addresses trough the wan-manager

## Release v1.32.6 - 2023-01-23(10:06:38 +0000)

### Fixes

- Move netlink code from IP-manager to NetDev

## Release v1.32.5 - 2023-01-16(08:38:50 +0000)

### Other

- [IP][Guest] Make a default IPv6 Configuration for the guest bridge.

## Release v1.32.4 - 2023-01-04(12:44:46 +0000)

### Fixes

- avoidable allocation with amxc_string_dup

## Release v1.32.3 - 2023-01-04(12:27:57 +0000)

### Fixes

- Don't set IPv4/6 netmodel flag if IP interface is of type Tunneled

## Release v1.32.2 - 2022-12-16(12:59:39 +0000)

### Fixes

-  [WANMode] Add default WANModes (Ethernet_DHCP and Ethernet_PPP)

## Release v1.32.1 - 2022-12-15(12:08:36 +0000)

### Other

- Make Type parameter persistent

## Release v1.32.0 - 2022-12-15(08:19:55 +0000)

### New

- [WANMode] Add default WANModes (Ethernet_DHCP and Ethernet_PPP)

## Release v1.31.1 - 2022-12-14(08:26:07 +0000)

### Fixes

- [prplOs compatibility] Don't change path and use ip utility according to installed packages

## Release v1.31.0 - 2022-12-14(08:12:25 +0000)

### New

- Adding two parameters to ip prefixes with the relative preferred lifetime and the relative valid lifetime

## Release v1.30.3 - 2022-12-09(14:40:56 +0000)

### Fixes

- Wrong ipv6 toggle when switching ipv6 interfaces after enabling of the pppmode in Wan-Manager

## Release v1.30.2 - 2022-12-09(13:54:46 +0000)

### Changes

- Make a default IPv6 Configuration for the guest bridge

## Release v1.30.1 - 2022-12-09(09:51:37 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.30.0 - 2022-12-08(15:11:13 +0000)

### New

- [TR181-IP] Add ipv4 and/or ipv6 flags to netdev interfaces in netmodel

## Release v1.29.1 - 2022-12-06(13:11:06 +0000)

### Fixes

- Disable ULA on LLA config

## Release v1.29.0 - 2022-11-28(15:39:34 +0000)

### New

- [tr181][ip] turn on ipv6address for IANA by default

## Release v1.28.1 - 2022-11-28(11:34:24 +0000)

### Fixes

- No LLA after firstboot

## Release v1.28.0 - 2022-11-17(10:53:31 +0000)

### New

- IP Manager DSLite defaults

## Release v1.27.1 - 2022-11-16(13:12:57 +0000)

### Other

- When the IP.Interface.2. (wan) is disabled, remove ipv6 addresses form interface [Fix]

## Release v1.27.0 - 2022-10-14(17:16:03 +0000)

### New

- First version of the Readme.md

## Release v1.26.7 - 2022-10-13(08:22:33 +0000)

### Other

- Improve plugin boot order

## Release v1.26.6 - 2022-10-12(07:47:27 +0000)

### Fixes

- The autodetection of the IP addresses can cause multiple duplicates

## Release v1.26.5 - 2022-10-06(07:57:39 +0000)

### Other

- disable import-dbg

## Release v1.26.4 - 2022-09-27(13:07:46 +0000)

### Other

- [IPv6] ULA Address on lan must be disabled by default.

## Release v1.26.3 - 2022-09-27(11:57:02 +0000)

### Fixes

- Autodetected IP addresses should be enabled when added

## Release v1.26.2 - 2022-09-26(12:41:55 +0000)

### Fixes

- Crash when deleting subscription

## Release v1.26.1 - 2022-09-21(16:54:57 +0000)

### Fixes

- IP-manager crash when deleting subscription

## Release v1.26.1 - 2022-09-21(16:47:15 +0000)

### Fixes

- IP-manager crash when deleting subscription

## Release v1.26.0 - 2022-09-14(13:58:29 +0000)

### New

- Add a new wan6 instance that will be used by the PPP wan-mode

## Release v1.25.0 - 2022-09-14(07:04:46 +0000)

### New

-  Add IPaddresses reported by NetDev to the DM if they do not yet exist

## Release v1.24.0 - 2022-09-02(07:28:48 +0000)

### New

- [TR181 IP] Add support for PPP

## Release v1.23.2 - 2022-08-05(13:43:37 +0000)

### Fixes

- Issues with dm functions not using internal structure

## Release v1.23.1 - 2022-07-26(10:47:18 +0000)

### Fixes

- Prefix is not reboot persistent for IPv6Addresses

## Release v1.23.0 - 2022-07-20(08:35:48 +0000)

### New

- Implement the stats object

## Release v1.22.11 - 2022-07-18(10:44:03 +0000)

### Fixes

- Some volatile data stays persistent if the IP-manager doesn't shutdown properly

## Release v1.22.10 - 2022-07-14(14:38:04 +0000)

### Fixes

- Unit tests are failing

## Release v1.22.9 - 2022-06-23(17:22:10 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v1.22.8 - 2022-06-17(09:55:12 +0000)

### Fixes

- Weird IP Addresses on interfaces after boot

## Release v1.22.7 - 2022-06-17(09:37:39 +0000)

### Other

- Add a "public-lan" default IPv4Address instance

## Release v1.22.6 - 2022-06-16(06:50:44 +0000)

### Fixes

- Use netdev-bound instead of netdev-up for NetDevName query

## Release v1.22.5 - 2022-06-11(06:35:04 +0000)

### Fixes

- Toggling IPv6 and setting MTUSize does not work on lla image

## Release v1.22.4 - 2022-06-10(08:43:05 +0000)

### Fixes

- Cleanup defaults

## Release v1.22.3 - 2022-06-10(06:33:47 +0000)

### Fixes

- Rework enable parameters and NetDevName change

## Release v1.22.2 - 2022-06-02(06:43:36 +0000)

### Fixes

- Make AddressingType writable again

## Release v1.22.1 - 2022-05-23(19:51:21 +0000)

### Fixes

- Clear interface name if query returns empty

## Release v1.22.0 - 2022-05-23(19:43:31 +0000)

### New

- Add a "public-lan" default IPv4Address instance

## Release v1.21.1 - 2022-05-19(06:58:38 +0000)

### Fixes

- Enable persistency

## Release v1.21.0 - 2022-05-17(09:59:38 +0000)

### New

- Save and load persistent parameters

## Release v1.20.3 - 2022-05-12(07:31:12 +0000)

### Other

- Add Unit tests

## Release v1.20.2 - 2022-05-12(06:23:19 +0000)

### Other

- Add missing define to expose `strdup`

## Release v1.20.1 - 2022-05-05(13:10:00 +0000)

### Fixes

- ULA Address missing

## Release v1.20.0 - 2022-05-04(15:53:33 +0000)

### New

- Add protected IP address parameters

## Release v1.19.0 - 2022-04-29(17:07:50 +0000)

### New

- Add netmodel queries to resolve the LowerLayers to NetDevName

## Release v1.18.2 - 2022-04-27(07:45:33 +0000)

### Changes

- set default ULA prefix

## Release v1.18.1 - 2022-04-26(07:02:32 +0000)

### Changes

- Use libipat functions to create IPv6 Addresses

## Release v1.18.0 - 2022-04-14(11:16:54 +0000)

### New

- expose IPv6Addresses which is triggered by a routerAdvertisement negotation

## Release v1.17.2 - 2022-04-12(13:43:24 +0000)

### Fixes

- DHCPv4 Address should come from NetModel query

## Release v1.17.1 - 2022-04-05(06:34:15 +0000)

### Fixes

- LastChange parameters doesn't reset when the status changes

## Release v1.17.0 - 2022-04-01(08:19:22 +0000)

### New

- Set IPv6 address/prefix status

## Release v1.16.3 - 2022-03-31(14:29:15 +0000)

### Fixes

- The loopback interface must be marked with the netdev flag.

## Release v1.16.2 - 2022-03-24(17:42:30 +0000)

### Fixes

- Wrong Status of DHCP IPv4Address

## Release v1.16.1 - 2022-03-24(10:59:32 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.16.0 - 2022-03-21(17:45:45 +0000)

### New

- Set the MTU value using UCI

## Release v1.15.1 - 2022-03-18(07:26:19 +0000)

### Fixes

- Status not working

## Release v1.15.0 - 2022-03-17(10:25:00 +0000)

### New

- [getDebugInformation] Install a getdebug information script in the prplOS

## Release v1.14.1 - 2022-03-04(14:15:30 +0000)

### Fixes

- Possible segfault do to not allocated timestamps

## Release v1.14.0 - 2022-03-04(09:54:31 +0000)

### New

- Create NetModel query for DHCP

## Release v1.13.0 - 2022-03-03(12:49:27 +0000)

### New

- [CONFIG] add lcm interface to config

## Release v1.12.4 - 2022-02-25(10:53:17 +0000)

### Other

- Enable core dumps by default

## Release v1.12.3 - 2022-02-15(22:14:40 +0000)

### Fixes

- IP-manager build fails on rpi3 build

## Release v1.12.2 - 2022-02-14(21:36:53 +0000)

### Fixes

- only the NetDev client should synchronize NetDevName

## Release v1.12.1 - 2022-02-09(09:06:39 +0000)

### Changes

- It should be possible to set multiple IPv4Addresses

## Release v1.12.0 - 2022-01-31(12:25:46 +0000)

### New

- It must be possible to set an IPv6 Address using the linux low level API

### Changes

- Use mod-ipm-netlink for IPAddresses the UCI module can't set

## Release v1.11.1 - 2022-01-20(07:50:26 +0000)

### Fixes

- IPv4/6 Enable and some other small features need fixing

## Release v1.11.0 - 2022-01-05(09:08:11 +0000)

### New

- Set IPv6 address in Linux

## Release v1.10.0 - 2021-12-23(15:40:03 +0000)

### New

- Support for IP.Interface.Enable and IP.Interface.IPv4Enable

## Release v1.9.0 - 2021-12-10(19:44:23 +0000)

### New

- Implement IPv6Address population

## Release v1.8.0 - 2021-12-09(08:19:47 +0000)

### New

- set flag "netdev" on NetModel interfaces

## Release v1.7.4 - 2021-12-01(12:05:05 +0000)

### Fixes

- Fix memory leaks

### Other

- [ACL] The IP Manager must have default ACL files configured

## Release v1.7.3 - 2021-11-29(15:42:40 +0000)

### Changes

- only reload network for static addresses

## Release v1.7.2 - 2021-11-29(08:59:16 +0000)

### Fixes

- Apply correct default configuration for a guest network

## Release v1.7.1 - 2021-11-22(07:48:35 +0000)

### Fixes

- Add mod-netmodel as a dependency

## Release v1.7.0 - 2021-11-18(14:53:05 +0000)

### New

- mod_netmodel populate netmodel hierarchy (Device.IP.)

## Release v1.6.2 - 2021-11-17(10:13:27 +0000)

### Other

- Implementation of unit tests (using cmocka)

## Release v1.6.1 - 2021-11-15(12:02:13 +0000)

### Other

- [CI] Disable g++ compilation

## Release v1.6.0 - 2021-11-15(10:41:58 +0000)

### New

- [IPManager] Provide (protected) API to set an IPv4 Address instance

## Release v1.5.1 - 2021-11-15(09:15:29 +0000)

### Fixes

- Missing mod-sahtrace dependecy in some components

## Release v1.5.0 - 2021-11-04(07:28:03 +0000)

### New

- [ip-manager] add guest config

## Release v1.4.1 - 2021-10-27(08:29:02 +0000)

### Other

- Fix unit tests

## Release v1.4.0 - 2021-10-25(15:55:35 +0000)

### New

- [TR181 IP manager] Manage a single Static IPv4Address on a fixed lan interface

## Release v1.3.1 - 2021-10-08(06:40:52 +0000)

### Changes

- [TR181 IP] Use amx transactions iso amxd_object_emit_changed

## Release v1.3.0 - 2021-09-27(14:54:52 +0000)

### New

- Add UCI "separation of concern" module for IPv4 address configuration

## Release v1.2.1 - 2021-09-15(07:12:33 +0000)
