include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C odl all
	$(MAKE) -C mod-ipm-uci/src all
	$(MAKE) -C mod-ipm-netlink/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C odl clean
	$(MAKE) -C mod-ipm-uci/src clean
	$(MAKE) -C mod-ipm-netlink/src clean
	$(MAKE) -C doc clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 odl/ip-manager-definition.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-ip-manager_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager_caps.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager_caps.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-interface.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-interface.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-interface_ipaddress_functions.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-interface_ipaddress_functions.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-activeport.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-activeport.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-ipv4addr.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-ipv4addr.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-ipv6addr.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-ipv6addr.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-ipv6prefix.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager-ipv6prefix.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0755 odl/ip-manager.odl $(DEST)/etc/amx/$(COMPONENT)/ip-manager.odl
	$(INSTALL) -D -p -m 0644 odl/global_ip_interfaces.odl.uc $(DEST)/etc/amx/modules/global_ip_interfaces.odl.uc
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipm-uci.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-ipm-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipm-netlink.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-ipm-netlink.so
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/debug_network.sh $(DEST)/usr/lib/debuginfo/debug_network.sh

package: all
	$(INSTALL) -D -p -m 0644 odl/ip-manager-definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-ip-manager_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager_caps.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-interface.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-interface.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-interface_ipaddress_functions.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-interface_ipaddress_functions.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-activeport.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-activeport.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-ipv4addr.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-ipv4addr.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-ipv6addr.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-ipv6addr.odl
	$(INSTALL) -D -p -m 0644 odl/ip-manager-ipv6prefix.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager-ipv6prefix.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0755 odl/ip-manager.odl $(PKGDIR)/etc/amx/$(COMPONENT)/ip-manager.odl
	$(INSTALL) -D -p -m 0644 odl/global_ip_interfaces.odl.uc $(PKGDIR)/etc/amx/modules/global_ip_interfaces.odl.uc
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipm-uci.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-ipm-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipm-netlink.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-ipm-netlink.so
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/debug_network.sh $(PKGDIR)/usr/lib/debuginfo/debug_network.sh
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += odl/ip-manager-definition.odl)
	$(eval ODLFILES += odl/ip-manager.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-ipm-uci/test run
	$(MAKE) -C mod-ipm-uci/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test